# Mailroom service for GitLab.com

Mailroom is a service that monitors a mailbox for incoming mail and sends the
message to redis, which is then processed by sidekiq.  This feature enables
GitLab to utilize email to supplement issues, comments, and merge requests with
data from an email message and enables the feature [Service Desk].  This has
been a feature since version 8.0.0 of GitLab and this currently runs on it's own
set of servers for GitLab.com. Documentation for this service and its
capabilities can be found [in our administration
documentation](https://docs.gitlab.com/ee/administration/incoming_email.html)

GitLab achieves this functionality using the [mail_room] ruby gem.

This readiness review covers the Mailroom service in it's current state and
improvements being done as we migrate from Virtual Machines into Kubernetes.

Operational Runbook for this service can be found in our runbooks project: https://gitlab.com/gitlab-com/runbooks/blob/master/troubleshooting/service-mailroom.md

## Table of contents

  * [Architecture overview](#architecture-and-overview)
  * [Configuration](#configuration)
  * [Risk assessment](#risk-assessment-and-blast-radius-of-failures)
  * [Security considerations](#security-considerations)
  * [Application upgrade/rollback](#application-upgrade-and-rollback)
  * [Observability and Monitoring/Logging](#observability-and-monitoring)
  * [Testing](#testing)

## Architecture and Overview

Mailroom is a stateless service that monitors an IMAP connection and creates an
entry in a Redis queue upon receipt of an email message.  The processing of the
email is performed by Sidekiq.

Note that we are configuring Mailroom to utilize our redis nodes that are
dedicated to sidekiq.  Our redis-cache nodes are not involved in this portion of
the configuration.

### Operational Diagram

```mermaid
sequenceDiagram
  participant email
  participant mail_room
  participant redis
  participant sidekiq

  mail_room->>email: read email

  activate mail_room
  mail_room->>redis: create job
  deactivate mail_room

  activate redis
  sidekiq->>redis: create job
  deactivate redis

  loop job
    sidekiq->>sidekiq: processing
  end
```

## Configuration

Mailroom is a feature of our GitLab Helm Chart.  Most configuration items are
stored under the keys `global.appConfig.incomingEmail`. The redis configuration
is shared amoung other components and can be found under `global.redis`.

Details of this configuration in helm chart are in the helm chart
[documentation](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/deployment.md#incoming-email)

GitLab.com utilizes an IMAP connection that requires credentials.  With the move
to Kubernetes, we utilize a Kubernetes Secret Object, as our chart
[documentation
describes](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/secrets.md#imap-password-for-incoming-emails)

GitLab.com utilizes authentication to our redis hosts.  With the move to
Kubernetes, we utilize a Kubernetes Secret Object, as our chart [documentation
describes](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/charts/globals.md#configure-redis-settings)

Since this is an existing service running on our current VM infrastructure with
an existing GitLab configuration, we'll ensure all settings that are stored in
our `gitlab.rb` file are carried over into our Kuberentes installation.  Any
configuration item that does not contain secrets live in the chef roles
`<environment-base>` under the keys
`default_attributes.omnibus-gitlab.gitlab_rb.gitlab-rails.incoming_email_*`.
Credentials are located in our encrypted databag inside of GKMS
`gitlab-omnibus-secrets` at the same key location.

### Canary

Currently, a "canary" email account that accepts incoming mail does not exist,
nor do we have the necessary namespacing provided by our sidekiq fleet. While we
can set up an email account for a canary mailroom service to monitor, we'll be
missing half the featureset of what Mailroom provides.  All outgoing production
email will contain a reply-to email address that won't route through canary.
The only features we'd be able to test Mailroom in canary is for setting up new
issues, merge requests, or items for [Service Desk].  Due to this the mailroom
service will not have a canary deployment.

## Risk Assessment and Blast radius of failures

This service supports being able to run multiple instances for redundancy
through the use of Arbitration in tandem with Redis.  This configuration
prevents duplicate messages being handled by multiple Mailroom services.  More
about this can be found on the [gem's
README](https://github.com/tpitale/mail_room#arbitration)

Currently our helm chart limits mailroom to only one replica with no available
autoscaling.  Issue https://gitlab.com/gitlab-org/charts/gitlab/issues/1663 was
created to address this.

Should this service fail, any incoming email would not be processed.  When the
service comes back online, any existing messages in the inbox will then be
processed as quickly as the service can.  The impact of this would cause a delay
in any message bound for comments, issues, merge requests, and any replies for
the [Service Desk] feature of GitLab.

## Resource Management

Currently we are using the default configuration of the mailroom chart which
does not enforce any limits, but only requests for resources.  The defaults in
our helm chart are
[documented](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/charts/gitlab/charts/mailroom/values.yaml#L41-47).

The low allocation of resources was based on the current utilization of the
mailroom VMs which has very low sustained CPU utilization:

* [mailroom-01](https://dashboards.gitlab.net/d/bd2Kl9Imk/host-stats?orgId=1&var-environment=gprd&var-node=mailroom-01-sv-gprd.c.gitlab-production.internal&var-promethus=prometheus-01-inf-gprd)
* [mailroom-02](https://dashboards.gitlab.net/d/bd2Kl9Imk/host-stats?orgId=1&var-environment=gprd&var-node=mailroom-02-sv-gprd.c.gitlab-production.internal&var-promethus=prometheus-01-inf-gprd)

## Security Considerations

### Network Access

The Pods running mailroom will require the following:

  * Outbound DNS
  * Outbound IMAP
  * Outbound Redis

A Network Policy is configured as noted here: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/commit/13b73e3e07b54619df67f651a2c745e74a25d7c5

### Abuse

We [already
document](https://docs.gitlab.com/ee/administration/incoming_email.html#security-concerns)
some security concerns for this service.  GitLab.com mitigates this by using the
domain `incoming.gitlab.com` for emails processed by mailroom.

## Application Upgrade and Rollback

```mermaid
sequenceDiagram
    participant Administrator
    participant GitLab.com
    Administrator->>GitLab.com: Submits an MR for review
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: branch mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Dryrun apply on all envs
    end
    Administrator->>GitLab.com: MR merged to master
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: master mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to non-production envs
    end
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to production
    end
```

These versions are set for the Mailroom service:

* `CHART_VERSION` which is set in the [configuration project](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/4dd5b9759bd475616f23573a94c4a42d2ba242cb/CHART_VERSION)
* Mailroom image version which is a [value in the chart](https://gitlab.com/gitlab-org/charts/gitlab/blob/f38c4b683ed770d47a87fc8c1011338eb6e24b88/charts/gitlab/charts/mailroom/values.yaml#L3)
* Or as an override in [k8s-workloads/gitlab-com]

There is no connection between specifying a version inside of our
[k8s-workloads/gitlab-com] project and what version our [CNG] project builds.  Due to this,
we must ensure if we decide to deploy a custom version of the mailroom image,
that it already exists.  Otherwise the deploy will fail.  Issue:
https://gitlab.com/gitlab-com/gl-infra/delivery/issues/388 has been created to
address this problem.

## Dogfooding

Wherever possible we try to leverage GitLab application features for managing
the deployments on Kubernetes. This includes:

* AutoDevops
* CI pipelines
* Package registry

These issues are tracked in the [Dogfooding Kubernetes features issue board](https://gitlab.com/groups/gitlab-org/-/boards/1284732?label_name[]=Delivery&label_name[]=Dogfooding&label_name[]=group%3A%3Aautodevops%20and%20kubernetes).

## Observability and Monitoring

The Cluster pods are monitored using Prometheus and is installed in the cluster
in its own namespace using the [prometheus operator helm
chart](https://github.com/helm/charts/tree/master/stable/prometheus-operator)

* There are multiple dashboards for monitoring both the GKE cluster and
  performance of the application:
  * [Workloads for PreProd](https://dashboards.gitlab.net/d/kubernetes-resources-workload/kubernetes-compute-resources-workload?orgId=1&refresh=10s&var-datasource=Global&var-cluster=pre-gitlab-gke&var-namespace=plantuml&var-workload=plantuml&var-type=deployment): Monitoring scaling and resources
  * [Pods for PreProd](https://dashboards.gitlab.net/d/kubernetes-resources-pod/kubernetes-compute-resources-pod?orgId=1&refresh=10s&var-datasource=Global&var-cluster=pre-gitlab-gke&var-namespace=plantuml&var-pod=plantuml-7f6b9b6894-nwzfm): Metrics for individual pods
  * [Pods for Mailroom](https://dashboards.gitlab.net/d/mailroom-pod/mailroom-pod-info?orgId=1&from=now-12h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-cluster=gstg-gitlab-gke&var-namespace=gitlab&var-Node=All&var-Deployment=gitlab-mailroom)
  * [Application Metrics for Mailroom](https://dashboards.gitlab.net/d/mailroom-app/mailroom-application-info)

The [mail_room] gem currently does not support exposing metrics for monitoring
the performance and state of this service.  Due to this, we rely on an in house
IMAP prometheus exporter: https://gitlab.com/ahmadsherif/imap-mailbox-exporter.
The IMAP exporter runs on our Stackdriver Exporter nodes.
https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/f43f39598afd78d2534f8da6ac84d639ed2cf7bf/roles/gprd-infra-sd-exporter.json#L38
This ensures that we continue to have metrics for the status of our email inbox
after the mailroom VMs have been removed from our infrastructure.  An issue was
created on the [mail_room] gem to potentially introduce a Prometheus endpoint to
rid of this special exporter: https://github.com/tpitale/mail_room/issues/99

Using this exporter we have existing alerting that we can reuse:
https://gitlab.com/gitlab-com/runbooks/blob/master/rules/mailroom.yml

We'll also have existing alerts provided by our Kubernetes Prometheus Operator
integration that we'll continue to utilize to monitor the health of the running
Pods in our infrastructure:
https://gitlab.com/gitlab-com/runbooks/blob/master/rules/kubernetes.yml#L28-73
  * If a Replicaset does not have the desired amount of Pods running
  * If Pods are not ready
  * If a Pod is stuck in a CrashLoop
  * If the Deployment does not have the desired amount of Pods running

## Responsibility

* Subject Manner Experts:
  * Kubernetes: [Infrastructure Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/)
  * Mailroom: [DevOps](https://about.gitlab.com/handbook/engineering/development/)

## Testing

Testing to validate the usability of Mailroom inside of Kubernetes was performed
on the preproduction environment.

## Logging

Currently logging in mailroom is nonexistant.  With the move to Kubernetes and
the inclusion of an upgrade of [mail_room] version 0.10.0, we'll have the
ability to view structured logs and ingest them into our existing logging
infrastructure.

Issue: https://gitlab.com/gitlab-org/gitlab/issues/35108 is opened to improve
the GitLab product and bring in this new version.

All logging for the GKE cluster and all of its service is handled by
StackDriver with a log sink to pubsub. Like with the existing infrastructure,
a pubsubbeat is used to consume logs from pubsub and forward them to
elasticsearch.

Logs can be found in Kibana:

* [Production Mailroom
  Logs](https://log.gitlab.net/goto/831c052e07212682926c6300987861c5)
* [Staging Mailroom
  Logs](https://nonprod-log.gitlab.net/goto/0d49c35a6033d7fe37657505341fec73)

## Readiness review participants

1. @henri
1.

[CNG]: https://gitlab.com/gitlab-org/build/CNG
[k8s-workloads/gitlab-com]: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com
[mail_room]: https://github.com/tpitale/mail_room
[Service Desk]: https://docs.gitlab.com/ee/user/project/service_desk.html
