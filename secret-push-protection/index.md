# Secret push protection

## Experiment

### Service Catalog

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [service catalog
  entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services)
for the service. Ensure that the following items are present in the
service catalog, or listed here:
  - Link to or provide a high-level summary of this new product feature.
    - We are currently in Phase 1 (Experimental), and we are adding
      secret detection to git push operations. This will prevent
detected secrets from ever making it to a repository.
    - The Phase 1 architecture involves no additional components and is
      entirely encapsulated in the Rails application server.
    - For further details, please read [Platform-wide Secret Detection Blueprint – Phase
      1](https://docs.gitlab.com/ee/architecture/blueprints/secret_detection/#phase-1---ruby-pushcheck-pre-receive-integration).
  - Link to the [Architecture Design
    Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/)
for this feature, if there wasn't a design completed for this feature
please explain why:
    - Please have a look at this [architecture diagram for phase
      1](https://gitlab.com/groups/gitlab-org/-/epics/11587#architecture)
and the [high-level architecture
diagram](https://docs.gitlab.com/ee/architecture/blueprints/secret_detection/#high-level-architecture)
from the blueprint.
  - List the feature group that created this feature/service and who are
    the current Engineering Managers, Product Managers and their
Directors:
    - The "~group::secret detection" team created this feature.
    - Engineering Manager is `@amarpatel`.
    - Product Manager is `@smeadzinger`
    - Senior Engineering Manager is `@twoodham`.
  - List individuals are the subject matter experts and know the most
    about this feature:
    - Feature development was led by `@ahmed.hemdan` with help from
      `@serenafang`, `@vbhat161`, and `@rossfuhrman`.
  - List the team or set of individuals will take responsibility for the
    reliability of the feature once it is in production:
    - Same as the feature development team.
  - List the member(s) of the team who built the feature will be on call
    for the launch:
    - Same as the feature development team.
  - List the external and internal dependencies to the application (ex:
    redis, postgres, etc) for this feature and how the service will be
impacted by a failure of that dependency:
    - The feature depends on: `workhorse`, `gitaly`, `rails`, and the
      `gitlab-secret_detection` ruby gem.
    - The gem contains the detection logic and has no external runtime
      dependencies. Failure of these dependecies could lead to
degradation or failure of the scanning.

### Infrastructure

_The items below will be reviewed by the Reliability team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related
  to this feature? If not, what kind of resources are not covered?

  N/A
- [x] Is the service covered by any DDoS protection solution (GCP/AWS
  load-balancers or Cloudflare usually cover this)?
    Covered by being a part of the rails monolith.
- [x] Are all cloud infrastructure resources labeled according to the
  [Infrastructure Labels and
Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/)
guidelines?
    Covered by being a part of the rails monolith.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] List the top three operational risks when this feature goes live.
1. git pushes are slowed down or timeout
2. git pushes degrade AND the web performance also degrades due to the
   push checks
3. the pre-recieve secret detection feature doesn't work properly, but
   does not affect git pushes or web performance (not
   sure this is considered an operational risk though)

- [ ] For each component and dependency, what is the blast radius of
  failures? Is there anything in the feature design that will reduce
this risk?
* the feature can be turned off via admin panel for Dedicated instances
  (primary audience for initial rollout)
* there is a feature flag for gitlab.com and SM instances
* we implemented filtering of blobs Gitaly is sending to us
  * only scan new files
  * do not scan binary files
  * do not scan files over 1Mib
* we implemented timeout(s) on the total scan time, as well as on
  individual blob scan times
* the push check can be skipped by adding `[skip secret detection]` to any
  commit message in the git push
* we implemented keyword filtering before doing regex scans to limit the
  number of expensive regex scans we need to do
* we have initially limited the number of regexes we are using
* we are running regex in subprocesses

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [metrics
  catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services)
for the service

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management
  issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/)
be used for rollout? If so, link to it here.

N/A
- [x] Can the new product feature be safely rolled back once it is live,
  can it be disabled using a feature flag?
    On Dedicated, this will be enabled/disabled via the
`/admin/application_settings/security_and_compliance` page.
    On gitlab.com and self-managed, this will be enabled/disabled via
the `pre_receive_secret_detection_push_check` feature flag. The feature flag can be enabled/disabled for the whole instance, or for a particular namespace.
    The only impact of disabling the feature is that there will no
longer be any pre-receive secret detection.
- [x] How are the artifacts being built for this feature (e.g., using
  the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image
building pipeline).

N/A

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects:
  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
  - Entry-points exposed to the internet (Public IPs, Load-Balancers,
    Buckets, etc...):
  - Other (anything relevant that might be worth mention):

N/A
- [x] Were the [GitLab security development
  guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html)
followed for this feature?
- [x] Was an [Application Security
  Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/)
requested, if appropriate? Yes.
[Link](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/223).
- [ ] Do we have an automatic procedure to update the infrastructure
  (OS, container images, packages, etc...). For example, using
unattended upgrade or [renovate
bot](https://github.com/renovatebot/renovate) to keep dependencies
up-to-date?
    There is currently no automated process for this. However, the next
Phase of the project will possibly involve moving away the current ruby
gem approach. Updating of the ruby gem dependencies should be addressed
during Phase II, assuming it is still relevant.
- [x] For IaC (e.g., Terraform), is there any secure static code
  analysis tools like ([kics](https://github.com/Checkmarx/kics) or
[checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC
is being introduced, please explain why.

N/A
- [x] If we're creating new containers (e.g., a Dockerfile with an image
  build pipeline), are we using `kics` or `checkov` to scan Dockerfiles
or [GitLab's
container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration)
scanner for vulnerabilities?

N/A

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

I think this entire section is N/A as we are using existing
infrastructure for this feature; no new services were added.

- [x] Are we adding any new forms of Authentication (New
  service-accounts, users/password for storage, OIDC, etc...)?

N/A
- [x] Was effort put in to ensure that the new service follows the
  [least privilege
principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege),
so that permissions are reduced as much as possible?

N/A
- [x] Do firewalls follow the least privilege principle (w/ network
  policies in Kubernetes or firewalls on cloud provider)?

N/A
- [x] Is the service covered by a [WAF (Web Application
  Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall)
in
[Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

N/A

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?
    We don't log customer data.
- [ ] What kind of data is stored on each system (secrets, customer
  data, audit, etc...)?
- [x] How is data rated according to our [data classification
  standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
(customer data is RED)?
    Data is rated orange as we do log secrets metadata, such as commit
sha, blob id, file paths, and line numbers.
- [x] Do we have audit logs for when data is accessed? If you are unsure
  or if using Reliability's central logging and a new pubsub topic was
created, create an issue in the [Security Logging
Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source)
using the `add-remove-change-log-source` template.
    There will be no new logs, so existing audit controls apply.
 - [x] Ensure appropriate logs are being kept for compliance and
   requirements for retention are met.
    N/A
 - [x] If the data classification = Red for the new environment, please
   create a [Security Compliance Intake
issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform).
Note this is not necessary if the service is deployed in existing
Production infrastructure.
    N/A

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to examples of logs on https://logs.gitlab.net:
  - [Production logs](https://log.gprd.gitlab.net/app/discover#/view/31afcbb2-28e9-466f-a6c3-486e869e1ee3?_g=())
  - [Logs for blocked pushes](https://log.gprd.gitlab.net/app/discover#/view/db7ba29d-d406-46df-8b43-e6d9c47fbed7)
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service:
  - [Dashboard](https://dashboards.gitlab.net/d/fdk7i56zibv28d/secret-push-protection-e28093-overview?orgId=1)
- [x] Sentry monitoring logs:
  - https://new-sentry.gitlab.net/organizations/gitlab/issues/?environment=gprd&project=3&query=is%3Aunresolved+ee%2Flib%2Fgitlab%2Fchecks%2Fsecrets_check.rb&referrer=issue-list&statsPeriod=14d

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

We only use the database to toggle the feature on/off in the instance project settings, so this section is N/A

- [ ] Are there custom backup/restore requirements?
- [ ] Are backups monitored?
- [ ] Was a restore from backup tested?
- [ ] Link to information about growth rate of stored data.

### Deployment

_The items below will be reviewed by the Delivery team._

- [] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
  - This feature uses pre-receive push options, which was introduced by [this commit](https://gitlab.com/gitlab-org/gitaly/-/commit/dd1303cdcf5219b7f9c2b0d764c28733ab370205) to Gitaly, so we require [Gitaly v16.11.2](https://gitlab.com/gitlab-org/gitaly/-/commit/1792e3adf2623a5316b673a1916bac9e1588f1c4) or later.
  - This feature uses [Gitlab 17.0](https://gitlab.com/gitlab-org/gitlab/-/releases/v17.0.0-ee)
- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
  - No
- [x] Will it be possible to roll back this feature? If so explain how it will be possible.
  - The feature can be rolled back by disabling the feature for individual projects through the UI or for all of Gitlab.com users by disabling the feature flag. To disable the feature for an individual project, follow the instructions [here](https://docs.gitlab.com/ee/user/application_security/secret_detection/secret_push_protection/#enable-secret-push-protection-in-a-project). To disable the feature for all .com users, open a change access request to disable the feature flag for the instance. More troubleshooting docs: https://handbook.gitlab.com/handbook/engineering/development/sec/secure/secret-detection/runbooks/secret-push-protection-troubleshooting/

### Security

_The items below will be reviewed by the InfraSec team._

- [x] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
  - An attacker could make a large number of pushes in rapid succession in a DoS attack. We have a [rate limit](https://docs.gitlab.com/ee/administration/settings/rate_limits_on_git_ssh_operations.html) of 600 pushes per minute to prevent this.
  - An attacker could push a large number of commits with many secrets to attempt to affect performance. To mitigate this, we do not scan files over 1Mib, and we implemented timeouts on the total scan time (60 seconds), as well as on individual blob scan times (5 seconds).
- [] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?
  - N/A

## General Availability

After releasing the feature in Beta, we made several [quality improvements](https://gitlab.com/groups/gitlab-org/-/epics/13107#details-epic-dashboard) to the feature. All the answers in previous maturity levels still hold valid considering the changes.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to the troubleshooting runbooks.

  You can find troubleshooting runbooks [here](https://handbook.gitlab.com/handbook/engineering/development/sec/secure/secret-detection/runbooks/secret-push-protection-troubleshooting/).

- [x] Link to an example of an alert and a corresponding runbook.

  There are no dedicated alerts configured for the feature since it is part of Rails monolith. 

- [x] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

  N/A. See the answer for the previous question.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to notes or testing results for assessing the outcome of failures of individual components.

  We have exhaustive [integration tests](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/spec/lib/gitlab/checks/secrets_check_spec.rb) added for the feature.

  In addition, we also have [E2E test](https://gitlab.com/gitlab-org/gitlab/-/issues/489863#note_2119390492) suite available for Staging environment and [plans](https://gitlab.com/gitlab-org/gitlab/-/issues/499255) to extend it to the production environment for more use cases.

- [x] What are the potential scalability or performance issues that may result with this change?

  Git-push events with significantly large amount of file changes, could impact Rails monolith resource consumption depending upon the GitLab environment since the feature loads all the pushed changes in the memory to run the secret detection scans on them.

- [x] What are a few operational concerns that will not be present at launch, but may be a concern later?

  1. Since the feature depends on Gitaly to retrieve committed files, degradation of Gitaly
  would impact the feature availability, in turn impacting git push events via CLI and Web.
  2. In case of large amount of files committed via git push, the feature could consume a significant chunk of memory when loading pushed changes to run the scan on them.

- [x] Are there any single points of failure in the design? If so list them here.

  The feature primarily depends on Gitaly service and database's availability, failure of either will impact the feature availibility. Apart from those, there are no other points of failure since the feature is embedded within Rails monolith infra than serving from a dedicated service.

- [x] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

  One of the worst-case failure scenarios is when the feature fails to handle the burst of traffic in GitLab.com. It would disrupt `git push` events for all the customers. Additionally, it could also degrade the performance of Gitaly service due to the feature's hard dependency on it.

  As a part of minimizing the blast-radius, we are preemptively filtering git blobs by several criteria like blob size, feature flag, text-only file types etc. in order to exit the feature early.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

We currently use the database to:

1. Toggle the feature on/off in the instance and project settings

2. Manage exclusions list when running Secret Detection scan.

- [x] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place? 

N/A

- [x] How does data age? Can data over a certain age be deleted?

Certain database entries of exclusions may get stale over time when there are changes in the source code related to the exclusions. For example, a path-based or raw value-based exclusion might become invalid when a file is moved or the raw value is removed, unless the customer diligently removes it from the exclusion list. We could run a scheduled job to cleanup stale entries, however, we haven't thought through it that far yet.

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

The implementation scope of the feature lie within the Rails monolith, hence the feature does not require Identity or Access-related management. 

- [x] Are we adding any new forms of Authentication (New
  service-accounts, users/password for storage, OIDC, etc...)?

  N/A

- [x] Was effort put in to ensure that the new service follows the
  [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

  N/A
- [x] Do firewalls follow the least privilege principle (w/ network
  policies in Kubernetes or firewalls on cloud provider)?

  N/A
- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall)
in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

  N/A

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).

  We conducted benchmarks for a fixed payload size against [2K Reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html) since we did not have metrics like average payload size, average/peak no. of commits available, particularly for GitLab Ultimate customers, since the feature is available only to them.

  The benchmarks and performance evaluation for SPP as a feature is available [here](https://gitlab.com/gitlab-org/gitlab/-/issues/431076).

- [x] Link to any load testing plans and results.

  Same as above.

- [x] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?

  The feature utilizes a Postgres database for git push event. We do not expect any potential performance impacts on the database due to the feature's database operations, as it only performs read operations (such as feature flag access and scan exclusions for the project) and does not involve any write operations.

- [x] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.

  We already have a [rate limit](https://docs.gitlab.com/ee/administration/settings/rate_limits_on_git_ssh_operations.html) of 600 pushes per minute to prevent malicious actors pushing large set of git push events.

- [x] Are there retry and back-off strategies for external dependencies?

  The feature is using the same Gitaly client used across Rails monolith which does not have retry strategy in place yet. 

- [x] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

  As mentioned above, we went ahead with the assumed metrics and we're confident the feature will be capable of serving for higher traffic.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  N/A
- [x] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?

  N/A since the feature is part of Rails monolith which follows [GitLab Release cycle](https://docs.gitlab.com/ee/policy/maintenance.html).

- [x] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?

  Yes, the deployment depends on gitlab.com since the feature is present within Rails monolith and the GitLab Secret Detection ruby gem that the feature currently uses is a vendored dependency within Rails monolith source code. 
