<!--
The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response.
_
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._
-->

# OpenBao for GitLab Secrets Manager

OpenBao is a component of [GitLab Secrets Manager](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_manager/) and is [a fork of HashiCorp Vault](https://openbao.org/) which [GitLab leads](https://github.com/openbao/openbao/blob/main/CONTRIBUTING.md#technical-steering-committee-tsc-members). As such, many items from the [Vault OSS Production Readiness plan](../vault/index.md) should apply here.

## Summary

OpenBao allows us to externalize secure storage of secrets from the core GitLab Rails component. Access to OpenBao can thus be constrained and audited independently of GitLab Rails. Additionally, we can use the existing Runner integration for Vault while building a more native experience for management of secrets.

After initial discussions with the infrastructure team, it was determined that self-service deployment via Runway would be a good initial approach to deploying OpenBao.

See the above [design document](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_manager/) for more information about architecture and previous ADRs around building a pure-Rails solution.

### Community Involvement

GitLab maintains a seat on the OpenBao [Technical Steering Committee](https://github.com/openbao/openbao/blob/main/CONTRIBUTING.md#technical-steering-committee-tsc-members) and our representative, Alex Scheel (@cipherboy-gitlab) is currently the TSC Chair, chair of the [Development Working Group](https://github.com/openbao/dev-wg), and [org-level maintainer](https://github.com/openbao/openbao/blob/main/MAINTAINERS.md#organization-level-maintainers) for the project. A [significant number of contributions](https://insights.lfx.linuxfoundation.org/foundation/lfedge/overview/github?project=openbao) come from GitLab directly.

This level of community involvement gives us less risk of unknown, breaking changes to the project occurring without our direct knowledge. While this is strictly an external, third-party project owned under the Linux Foundation, it functions more like a first-party project with GitLab driving nearly all significant changes.

### Differences over Vault Deployment

OpenBao will be deployed only for use with Pipeline secrets management. It will not replace the existing Vault deployment used elsewhere internally.

#### Ingress

As discussed in the design document above, OpenBao will be listening on a public network interface and runners will have direct access to it. We can run this behind [Runway load balancer](https://docs.runway.gitlab.com/runtimes/cloud-run/services/).

#### Storage

The existing Vault deployment uses Raft. While a suitable backend from an upstream compatibility perspective (HashiCorp through Vault Enterprise supports only Raft as a storage backend), the Pipeline Security team felt that GitLab as a whole had more experience with PostgreSQL. Additionally, Runway did not support persistent volumes so Raft's storage model would not work well; using a [external PostgreSQL instance](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/374#note_2235571614) was thus preferable.

We will need credentials to this PostgreSQL instance injected into the OpenBao containers at runtime.

#### Authentication

As discussed in the design document, Rails (Puma, Sidekiq) will self-issue JWT tokens to manage OpenBao (performing privileged operations like ACL modification) and will issue JWTs for Pipelines to access it.

No net-new secrets are required for deployment of the Rails side of the integration as the JWT token issuer already exists for OIDC ID Token creation.

## Experiment

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the all of the fields are populated.

  Runway integrates with [service catalog](https://docs.runway.gitlab.com/reference/observability/).



### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
   - Runway uses IaC for [provisioning](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) and [deploying](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/tree/main/reconciler?ref_type=heads) infrastructure.
   - We plan to deploy OpenBao [using Runway](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/merge_requests/352)./
   - The infrastructure team will [provision space](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/374) on [the Secure/Govern database](https://gitlab.com/groups/gitlab-org/-/epics/13043).
   - OpenBao will use keys in the [central Vault's Transit mount](https://gitlab.com/gitlab-org/gitlab/-/issues/499607) and will thus need credentials provisioned. Because Transit support automatic key upsert with sane defaults, we shouldn't need to provision keys explicitly.
   - We'll provision the initial Rails JWT engine and ACL policy with IAC and then refine it later via Rails to minimize setup differences for eventual self-hosted deployments.
- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?
   - Runway services exposed to the Internet are protected against DDoS attacks by GCP Cloud Armor. Additionally, Runway services can opt-in to be served via Cloudflare for an additional layer of protection. Runway services fronted by Cloud Connector are protected against DDoS attack by Cloudflare.
   - We will use the default from Runway: All Google Cloud projects that include an external Application Load Balancer or an external proxy Network Load Balancer are automatically enrolled in Google Cloud Armor Standard.
- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?
   - Runway requires [metadata](https://schemas.runway.gitlab.com/RunwayService/#metadata) to automatically label all cloud infrastructure resources.
   - Thus, Runway [provides the required resource labeling](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/merge_requests/352#note_2076027028).

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [x] List the top three operational risks when this feature goes live.
   - Availability: An outage in OpenBao would mean any Pipelines which use the native GitLab Secrets Manager would be unable to execute. Similarly, any user attempting to interact with the secrets manager pages (list secrets, ... etc) would see errors. This should not result in a complete outage of all of GitLab Rails. New deployments of Rails should be able to continue as well, because there are no dependencies on a live OpenBao instance.
   - Security: while we will limit access to CI Pipelines, these secrets can still be of significant operational risk to customers, e.g., if they contain signing or release deployment keys. We [have plans for improving tenant isolation](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_manager/#multi-tenancy-through-namespaces) which are [in-progress upstream](https://github.com/openbao/openbao/issues/787).
   - Scalability: OpenBao currently only supports Vault OSS's HA mode but does not yet support horizontal scalability; this is on [the project's roadmap](https://github.com/openbao/openbao/issues/569) to address.

- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?
   - Seal outage: [seal health checks](https://github.com/openbao/openbao/blob/main/vault/seal_autoseal.go#L586) could lead to an inability to rotate the active node in the event it goes down. We have plans to address [this upstream](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_manager/#multi-unseal-capabilities) by adding support for multiple unseal mechanisms.
   - PostgreSQL Storage outage: OpenBao will be unable to process requests and thus impact GitLab Rails as discussed above.
   - OpenBao: Discussed above in availability.
     - This includes components such as Runway and our GitLab-internal Vault instance.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service
   - Runway integrates with [metrics catalog](https://docs.runway.gitlab.com/reference/observability/). Setup TBD.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
   - To my knowledge, this should not require any downtime. Runway deployment can be done ahead of Rails deployment and a feature flag can be used to control enablement of this feature on GitLab Rails.
- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?
   - Runway uses continuous deployments and supports deploying reverted MRs. Runway automatically [rolls back](https://docs.runway.gitlab.com/guides/deployment-strategy/#auto-rollback) deployments experiencing elevated error rates.
   - Yes; data is self-contained in OpenBao and the feature is intended to be gate-able within GitLab Rails based on certain constraints (pricing tier). Rolling back the feature flag for enablement will be sufficient for disabling this on GitLab.Com.
- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).
   - Runway deploys [container images](https://docs.runway.gitlab.com/workloads/services/#container-image) stored in GitLab Container Registry and replicated in GCP Artifact Registry.
   - Distribution issues: https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/1674.
   - OpenBao upstream builds on RHEL 9.x UBI images which we will deploy on top of. We can replace the upstream goreleaser configuration for internal builds to later get [FIPS support](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/435).

See above section on [_Community Involvement_](#community-involvement). OpenBao aims to [maintain API compatibility](https://openbao.org/docs/policies/migration/#proposal) with HashiCorp Vault. This should mean our existing Runner integration (with Vault) will continue to function against OpenBao indefinitely unless breaking changes to the API occur from HashiCorp's side.

We maintains tests in GitLab Rails ([a](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/spec/models/secrets_management), [b](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/spec/lib/secrets_management), [c](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/spec/requests/api/graphql/secrets_management), [d](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/spec/services/secrets_management)) which test functionality against the OpenBao instance. New OpenBao release [brought internally](https://gitlab.com/gitlab-org/govern/secrets-management/openbao-internal) can be [validated through GDK](https://gitlab.com/gitlab-org/govern/secrets-management/openbao-internal) before rolling out via Runway. The internal patch system can be used to quickly turnaround any changes we need to OpenBao in production, if incompatibility or bugs are discovered.

OpenBao maintains a [changelog](https://github.com/openbao/openbao/blob/main/CHANGELOG.md) and [release notes](https://openbao.org/docs/release-notes/) which highlight any potentially breaking changes. As highlighted elsewhere, the project uses Dependabot for its own dependency updates.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [ ] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects: Runway
    - Runway workloads that are not deployed to external GCP projects are deployed to the following projects:
      - [`gitlab-runway-staging`](https://console.cloud.google.com/welcome?project=gitlab-runway-staging)
      - [`gitlab-runway-production`](https://console.cloud.google.com/welcome?project=gitlab-runway-production)
  - New Subnets: [Handled by Runway](https://console.cloud.google.com/networking/networks/details/runway-staging?project=gitlab-runway-staging&pageTab=SUBNETS)
  - VPC/Network Peering: Automatically provided by Runway.
  - DNS names:
    - `secrets.gitlab.com` (suggested)
    - `secrets-manager.staging.runway.gitlab.net`
    - `secrets-manager.runway.gitlab.net`
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...): Runway GCP L-B.
    - We are not using Cloud Connector.
  - Other (anything relevant that might be worth mention):
- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
  - OpenBao was a continuation of the existing HashiCorp Vault project and uses [CodeQL](https://github.com/openbao/openbao/pull/603) for upstream scanning.
  - The Rails components were developed with the security development guide in mind.
- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
  - Pentest request: https://gitlab.com/gitlab-com/gl-security/product-security/pentest-coordination/-/issues/2
  - Threat model: https://gitlab.com/gitlab-com/gl-security/product-security/appsec/threat-models/-/blob/master/gitlab-org/gitlab/gitlab-secrets-manager.md
- [x] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?
  - Runway automatically uses semantic version for [releases](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/releases). Service owners can manually upgrade, or automatically upgrade using `renovate_bot`.
  - The upstream project uses Dependabot per LF guidance: https://github.com/openbao/openbao/blob/main/.github/dependabot.yml
  - Internally we mirror that repository as [openbao-mirror](https://gitlab.com/gitlab-org/govern/secrets-management/openbao-mirror) and have the ability to apply patches via [openbao-internal](https://gitlab.com/gitlab-org/govern/secrets-management/openbao-internal), our build system which uses our preferred Go toolchain.
- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.
  - There should be minimal net-new IaC contributions from this and deployment for the core OpenBao component will use Runway.
  - One new contribution will be the use of TF to provision the [initial GitLab Rails JWT method and ACLs](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_manager/#authentication).
- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?
  - Runway uses checkov for [provisioning](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/.tool-versions) and [deploying](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/main/.tool-versions?ref_type=heads) infrastructure.
  - We build on top of upstream's images, which use Dependabot to manage updates to containers. We can revise this with later guidance ahead of production use if Distribution wishes.

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?
  - OpenBao will use the `patroni-sec-v16` database; credentials will be injected from our internal Vault instance into the Runway container at runtime as environment variables.
  - Openbao will use the same credentials to access [a Transit mount](https://gitlab.com/gitlab-org/gitlab/-/issues/499607) for auto-unseal capabilities.
  - Runway internally creates new identities: a [set of service accounts](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/modules/runway_service/service_accounts.tf) and an [OIDC workload identity pool](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/modules/deployment_project/oidc.tf).
- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?
  - Yes, as discussed in the [design document](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_manager/) and [threat model](https://gitlab.com/gitlab-com/gl-security/product-security/appsec/threat-models/-/blob/master/gitlab-org/gitlab/gitlab-secrets-manager.md).
- [x] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?
  - Only port 8200 will need to be externally accessible.
- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?
  - [Per discussion](https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/234#note_2344699055), GCP Cloud Armor provides WAF rules.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?
  - OpenBao uses [HMACing of strings](https://openbao.org/docs/audit/#sensitive-information) to redact while preserving auditability.
- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
  - OpenBao stores secrets and uses key material from GCP to retrieve encrypted records from PostgreSQL.
- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard/) (customer data is RED)?
  - Nearly all data is RED.
- [x] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
  - [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
  - [x] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.
    - Security Assurance: https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/59
  - OpenBao can create audit logs using the following plugins: https://openbao.org/docs/audit/; will file an intake issue.
  - Intake issue: https://gitlab.com/gitlab-com/gl-security/security-operations/security-logging/security-logging/-/issues/515#note_2322020733

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to examples of logs on https://logs.gitlab.net
  - Runway uses GCP Cloud Logging for system and application logs.
    - [gitlab-runway-staging](https://console.cloud.google.com/logs/query?project=gitlab-runway-staging)
    - [gitlab-runway-production](https://console.cloud.google.com/logs/query?project=gitlab-runway-production)
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.
  - Runway automatically generates [dashboards](https://dashboards.gitlab.net/d/runway-service/runway3a-runway-service-metrics) for service overview.
    - https://dashboards.gitlab.net/d/secrets-manager/secrets-manager3a-overview <!-- TBD: is this the right service ID? -->

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there custom backup/restore requirements?
- [ ] Are backups monitored?
- [ ] Was a restore from backup tested?
- [ ] Link to information about growth rate of stored data.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
- [ ] Will it be possible to roll back this feature? If so explain how it will be possible.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the troubleshooting runbooks.
- [x] Link to an example of an alert and a corresponding runbook.
  - Runway automatically generates [alerts](https://docs.runway.gitlab.com/reference/observability/#alerts) for errors, apdex, and traffic cessation. Runway automatically generates alerts for [saturation resources](https://docs.runway.gitlab.com/reference/capacity-planning/).
    - https://dashboards.gitlab.net/d/alerts-sat_runway_container_cpu/e0a90721-e5ee-5feb-aed8-11cbff05b7ee?var-PROMETHEUS_DS=mimir-runway&var-type={+Runway Service ID+}
    - https://dashboards.gitlab.net/d/alerts-sat_runway_container_memory/f4132aba-3db6-5019-b238-e0b0d481596b?var-PROMETHEUS_DS=mimir-runway&var-type={+Runway Service ID+}
- [x] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.
  - Runway uses GCP projects that on-call SREs have access to.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?
  - Runway automatically [scales instances](https://schemas.runway.gitlab.com/RunwayService/#spec_scalability_min_instances) based on traffic.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [x] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
  - Runway automatically [rolls back](https://docs.runway.gitlab.com/guides/deployment-strategy/#auto-rollback) deployments experiencing elevated error rates. Runway supports [healthchecks](https://docs.runway.gitlab.com/guides/healthchecks/) for startup and liveness probes.
- [x] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
  - Runway automatically depends on GitLab.com. Runway optionally [deploying from ops](https://docs.runway.gitlab.com/guides/deploying-from-ops/) for critical services.

