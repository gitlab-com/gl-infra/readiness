# Windows Shared Runners -- BETA

## Summary

We will be providing autoscaling Windows CI runners in a similar manner to our Linux ones.
These will be free during the beta period and will be available to all GitLab users.

## Architecture

- The new Autoscaler for Windows Shared Runners will create/run/destroy a VM on GCP per job using the [custom executor](https://docs.gitlab.com/runner/executors/custom.html).
- The VM created on GCP will be based on our own custom VM that has Docker installed, common tools and dependencies required by the Runner. link https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers/tree/master/cookbooks/preinstalled-software#preinstalled-software
- The inclusion of common tools will reduce build time for users.
- The Windows Shared Runners will only run jobs with the appropriate Windows tag.
  - Issue [#8902](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8902) discusses which tags we will be using.

DIAGRAM TO GO HERE

## Operational Risk Assessment

These runners will potentially suffer from similar problems that we experience on our Linux runner infrastructure.

Examples:

- GCP API quota issues, relevant current quotas:
  - API request 2000 per second
  - 2400 CPUs
  - 204,800 GB standard disk
  - 2300 in-use IP addreses

We have no way to do zero downtime deploys or patches during the beta. This is ongoing work in
[gitlab-org/gitlab-runner#4917](https://gitlab.com/gitlab-org/gitlab-runner/issues/4917). We will
be gathering data on latencies and downtimes during upgrades once we are gathering relevant data
into prometheus.

## Security

Security is of particular concern when running Windows. As such, we will be patching the runner managers regularly.
As of the beta, patches to the runner managers will cause downtime as we have no way to do zero downtime upgrades.
The images that the executor machines run will also be rebuilt on a regular basis with the most recent patches.
Discussion on automating the executor image can be found in [gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers#4](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers/issues/4).

As with the Linux runners, each runner machine is used for only one job and then recycled.

We have done our utmost to ensure that any potential breach of the runners would be minor. Ultimately, we let anyone
run arbitrary code as an administrator on these runners. Thus we have ensured that the runner machines have no access
to the local network and cannot speak back to the runner manager itself. All of the runners are behind NAT and do
not have public IPs. All of the above are in a completely separate project from Linux CI.

The managers will not be accessible from the internet. Should someone need to log into the machines there will be
a process defined to do so. The machines will be connected to Okta ASA and authorized individuals will be able
to log in via RDP.

## Monitoring and Alerts

We will be monitoring the managers via the [wmi_exporter](https://github.com/martinlindhe/wmi_exporter). This will provide
us with similar information as the `node_exporter` does on Linux. During the beta, alerting will be limited as we create
necessary documentation and training procedures for individuals on-call.

Logging?

There will be no SLA for the beta period.

We will update status.io with any potential downtime events, but we will not be tweeting about it during the beta period.

## Testing

We have done an internal beta test with the runner managers and it works as expected. They will remain connected
to the ops instance and staging once we go to public beta, but will be removed from the `gitlab-com` and `gitlab-org`
groups.

We do CI tests on the runner images for every merge request to ensure that the resulting image will be functional.
