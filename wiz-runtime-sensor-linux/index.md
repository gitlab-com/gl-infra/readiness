The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

Wiz Runtime Sensor is deployed using [chef cookbook](https://gitlab.com/gitlab-cookbooks/gitlab-wiz-sensor) that was already deployed on the Pre environment [MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/4945) to monitor malicious actions that run on VMs. Wiz Runtime Sensor internal document is added [here](https://internal.gitlab.com/handbook/security/product_security/infrastructure_security/tooling/wiz/).

The end goal is to deploy the Wiz Linux Sensor on all the VMs that would follow the definition of [What is considered Production?](https://gitlab.com/gitlab-com/gl-security/security-assurance/team-commercial-compliance/compliance/-/blob/master/production_definition.md#what-is-considered-production). This includes staging, ops and gprd environments.


## GA

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the all of the fields are populated.

Already add the service in the service catalog, initally it was only available on the kubnernetes clusters and now are extending it to the Linux VMs.

Overview of the architecture can be found in the [Internal Doc](https://internal.gitlab.com/handbook/security/product_security/infrastructure_security/tooling/wiz/#22-architecture)

### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?

    It would be deployed using the [chef cookbook](https://gitlab.com/gitlab-cookbooks/gitlab-wiz-sensor) created for the `wiz sensor` deployment.

- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

According to [Wiz docs](https://docs.wiz.io/wiz-docs/docs/wiz-sensor?lng=en#network-outage-resilience), if the Sensor loses connection to the Wiz portal, it will first attempt to reconnect for one minute. If that is unsuccessful, the Sensor has the following fallback methods:

1. Rule-Engine Detections: The Sensor will retry connecting hourly. After 24 hours offline, detections are considered outdated and removed locally.
2. Compromise Detections: After 24 hours offline, stored indicators of compromise (like file hashes) are deleted. Future encounters will re-check with the Wiz Reputation Service.
3. Runtime Execution Data (RED): RED is sent to the portal and stored locally. If the connection is lost, the Wiz agentless scanner can retrieve up to 48 hours of backed-up RED.

- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

    Wiz Runtime Sensor is the agent that is deployed on the Virtual Machines and is not related to any of the service, also there would be no seperate environment created for add the agents, hence separate labeling does not make sense

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [x] List the top three operational risks when this feature goes live.

    Wiz sensor is installed as daemon on the virtual machines and the CPU and Memory limits are enforced automatically by the wiz sensor. So there is no much operational risk with it.

    Below is how CPU and Memory limits are enforced.

    **CPU Limits**

    The limit is set dynamically according to the number of CPU cores on the machine. The Sensor uses up to 1% of each CPU core on the machine, while a minimum of 30% of a single CPU core is always reserved. Given a machine has X CPU cores, here is the formula used to determine the cgroup limit, in fractions of CPU cores:

    cpu_limit_in_cores = max(0.3, 0.01 * X)

    The CPU limit is the higher value of either:

    0.3 CPUs, allowing the Sensor to perform operations that don't scale with CPU count (i.e. communication with the Wiz portal, loading Threat Detection Rules, and initialization).
    
    1% of total CPUs, allowing the Sensor CPU usage to scale on large machines.
    
    Examples:

    A machine with 4 CPUs will have a CPU limit of 0.3 CPUs.
    A machine with 200 CPUs will have a CPU limit of 2 CPUs.

    **Memory Limits**

    The limit is set dynamically according to the number of CPU cores on the machine. The Sensor uses up to 10MiB of memory per CPU core on the machine, while a minimum of 300MiB are always reserved. Given a machine has X CPU cores, here is the formula used to determine the cgroup limit, stated in Mib:

    mem_limit_in_mib = max(300, 10 * X)

    The memory limit is the higher value of either:

    300MiB, allowing the Sensor to perform operations that don't scale with CPU count (i.e. communication with the Wiz portal, loading Threat Detection Rules, and initialization).
    
    10MiB of memory per machine CPU, allowing the Sensor CPU usage to scale on large machines.
    
    Examples:

    A machine with 4 CPUs will have a memory limit of 300MiB
    A machine with 200 CPUs will have a memory limit of 2GiB, roughly equivalent to 2000MiB

    We can also customize the CPU and Memory limits using the environment variables to override the dynamic values.

- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

    The agent failure should not create an operational risk, but as a result, no events/alerts are sent to the SIEM. That will severely limit our Security visibility and our ability to respond to Security Incidents.

    Before sending this data to the backend, the sensor will redact items such as Personally Identifiable Information (PII) or secrets, including passwords, tokens, certificate data, email addresses, JWT tokens, and authentication URLs. However, suppose a potentially harmful command is executed, and it exposes confidential information that fails to be redacted. In that case, the sensor may send the metadata and concealed confidential data to the Wiz Platform.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

    Already added to the metrics catalog, recently we modified it to add VM metrics for the Wiz sensor.

    https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/wiz-runtime-sensor.jsonnet


### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

    Not created for `gprd` yet but it would be created at the time of deployment, [there is one](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18391) for the `pre` environment, [one](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18518) for the `gstg` environment and [one](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18582) for the `ops` environment.

- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

    N/A as this is not a product feature but there are steps documented to [disable the sensor](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/wiz-runtime-sensor/README.md#disable-wiz-runtime-sensor-for-linux)

- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

    N/A as it id deployed using the cookbook that is hosted in the GitLab cookbooks repository.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects:
  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  - Other (anything relevant that might be worth mention):

    No new infrastructure would be added, just the agent would be installed on the virtual machines.

- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

    N/A This is vendor tool and we do not have control on the source code

- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.

     We don't have access to the code since this is a third-party vendor. But there was a Third Party Risk Management review done by the Security Assurance team which can be found here: https://gitlab.com/gitlab-com/gl-security/security-assurance/security-risk-team/third-party-vendor-security-management/-/issues/1946

- [x] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

    It would be deployed under the `chef-repo`, where the version bump MR are autocreated if there is any modification in the cookbook.

- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

    N/A No Infrastructure is added so there is no SAST scan available, but we have cookstyle and kitchen tests added to the repository to test the changes.

- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

   N/A No Infrastructure is added so there is no SAST scan available, but we have cookstyle and kitchen tests added to the repository to test the changes.

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

    N/A we are not adding any new forms of authentication. However the sensor would need user/password (generated by Wiz Portal) to connet to Wiz backend. Those are stored in vault and are referenced from vault.

- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

    The agent needs to access to the vault for specific path, the service account has been granted the necessary permissions to access specific secrets path. Apart from that there is no other access required.

- [x] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?

    N/A no infrastructure is added/modified so there is no need to make any changes, existing firewall rules works fine.

- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

    Wiz Runtime Sensor is the agent that is deployed on the Virtual Machines and is not a public facing service. As it is not a public facing service and not expose any public facing endpoints, it would not require us to add the DDoS/WAF solutions.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?

    N/A the service does not take any customer data.

- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

    Wiz Sensor would scan the system calls and the detections would only send metadata to Wiz Portal. The alerts are generated in Portal and would transitiont to SIEM, so there are no secrets stored / transition to Wiz Portal or the SIEM

- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard/) (customer data is RED)?

    The [data classification for the Wiz SaaS/Backend is RED](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml?ref_type=heads#L5225). The different Wiz Scans will redact data before sending it to the the Wiz Backend, but since there's a non-zero risk that it might fail, we still treat the system as RED data holder. 

    The Wiz Linux sensor stores temporary data on the VM disk and has [high privileges](https://internal.gitlab.com/handbook/security/product_security/infrastructure_security/tooling/evaluation-docs/2024-01-wiz-sensor-permissions-investigation/#4-linux-capabilities) on VMs that contain RED data. So, the scope of the data access should be considered RED.

- [x] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

    N/A

- [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.

    The logs are transfered to Wiz backend Portal and from the portal the alerts are transitioned to Devo

- [x] ~If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.~

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] ~Link to examples of logs on https://logs.gitlab.net~
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

    The Dashboards are created using the metric catalog as well as outside the metric catalog and below are the links.

    [Wiz Sensor Process](https://dashboards.gitlab.net/d/wiz-runtime-sensor-main/wiz-runtime-sensor3a-overview?orgId=1&var-PROMETHEUS_DS=e58c2f51-20f8-4f4b-ad48-2968782ca7d6&var-environment=pre) (using metrics catalog)

    [Global Dashboard](https://dashboards.gitlab.net/d/edvdfvtp4ms5cf/wiz-sensor-linux?orgId=1&refresh=5m) This dashboard gives us the global view of the top 2-5 CPU and Memory usage statistics and the coverage

    [Host Specific Dashboard](https://dashboards.gitlab.net/d/ddvdhgq0y914wf/wiz-sensor-linux-hosts?orgId=1&refresh=5m) This dashboard allows us to review the specific node-level CPU and Memory usage by wiz sensor to isolate any issues with particular node

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [x] ~Are there custom backup/restore requirements?~
- [x] ~Are backups monitored?~
- [x] ~Was a restore from backup tested?~
- [x] ~Link to information about growth rate of stored data.~

    N/A, this is vendor developed scanner that is being used to scan the realtime malicious actions so it is not applicable here.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

    We have created one for `Pre` and deployed it on the `Pre`
    
    Artefacts: [Pre change management issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18391) / [Pre MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/4945)

- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?

    N/A

- [x] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?

    No, but we did Performance Tests to understand the Sensor behavior under load. The results can be found [here](https://internal.gitlab.com/handbook/security/product_security/infrastructure_security/tooling/evaluation-docs/2024-01-wiz-sensor-performance-tests/#13-wiz-sensor-for-linux).

- [x] Will it be possible to roll back this feature? If so explain how it will be possible.

    N/A as this is not a product feature but there are steps documented to [disable the sensor](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/wiz-runtime-sensor/README.md#disable-wiz-runtime-sensor-for-linux)

### Security

_The items below will be reviewed by the InfraSec team._

- [x] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [x] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

    These sensors mainly hook itself to eBFP probes and listen to the syscalls and send the alerts to the Wiz backend. The risk is the binary is downloaded from the vendor and if the binary has any flaws it might impact us.

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to the troubleshooting runbooks.

    https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/wiz-runtime-sensor/README.md#wiz-sensor-service-linux

- [x] Link to an example of an alert and a corresponding runbook.

    There are auto generated alerts by metrics catalog and those should be enough.

- [x] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

    It is installed via cookbook and chef to all the nodes, so on-call would have the complete access to the service.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [x] ~Link to notes or testing results for assessing the outcome of failures of individual components.~
- [x] ~What are the potential scalability or performance issues that may result with this change?~
- [x] What are a few operational concerns that will not be present at launch, but may be a concern later?

    If the wiz-sensor accumulates many events and doesn't flush them, it could lead to OOM. This is still mitigated by the resource limits enforced by cgroups.

- [x] ~Are there any single points of failure in the design? If so list them here.~
- [x] ~As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?~

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [x] ~Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?~
- [x] ~How does data age? Can data over a certain age be deleted?~

    N/A, the security alerts would be sent to Devo

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [x] Link to any load testing plans and results.
- [x] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [x] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [x] Are there retry and back-off strategies for external dependencies?
- [x] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

    The deployment has CPU and memory limits are enfored using cgroups and hence we do not expect it to create issues. 

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

    The Change Management Issue is not created yet but it would be used for deployment 

- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?

    Frequent of OOM kills should trigger us to rollback the sensor and work with vendor to understand if it requires more memory or are there any unknown bugs in the sensor

- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?

      N/A