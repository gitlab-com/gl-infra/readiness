
# AI-Gateway

## Experiment

### Service Catalog

_The items below will be reviewed by the Reliability team._

The link to the ai-gateway in the service catalog is [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/services/service-catalog.yml#L671-691). The link to the architecture design for ai-gateway is [here](https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc/architecture/blueprints/ai_gateway). The in-progress architecture design for the Runway Platform (that ai-gateway uses) is [here](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129124).

The ai-gateway feature/service is owned by the [AI Framework group](https://about.gitlab.com/handbook/engineering/development/data-science/ai-framework/) in the AI Powered stage.

Their production and engineering managers and directors are
* [Michelle Gill](https://about.gitlab.com/company/team/#m_gill)
* [David O'Regan](https://about.gitlab.com/company/team/#oregand)
* [Torsten Linz](https://about.gitlab.com/company/team/#tlinz)
* [Taylor McCaslin](https://about.gitlab.com/company/team/#tmccaslin)

The individuals who know most about this feature are
* [Tan Le](https://gitlab.com/tle_gitlab)
* [Alexander Chueshev](https://gitlab.com/achueshev)
* [Andras Herczeg](https://gitlab.com/AndrasHerczeg)

The teams/individuals who will be responsible for the reliability of this feature are
* The Platforms scalability team (for issues related to runway, the platform that ai-gateway runs on top of)
* [AI Framework group](https://about.gitlab.com/handbook/engineering/development/data-science/ai-framework/) for the service itself

The application currently has no dependencies beyond running on top of the [Runway Platform](https://about.gitlab.com/handbook/engineering/infrastructure/platforms/tools/runway/). While failure of most parts of runway won't affect the service (as these parts are focussed around the safe delivery of updates to the service), failure of the underlying runtime used to run code in Runway will affect the service.

### Infrastructure

_The items below will be reviewed by the Reliability team._

- [ ] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?

No. The resources in the `unreview-poc-390200e5` project were created in an ad-hoc manner. We've been importing them into our IaC stack at
https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/tree/master/environments/ai-assisted-legacy-prd, but many are still pending, including notably the `ai-assist` and `ai-assist-test` GKE clusters
where the legacy deployment lives. The resources for the Runway deployment are all in IaC

- [ ] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

Yes, via [a CloudFlare load balancer](https://dash.cloudflare.com/852e9d53d0f8adbd9205389356f2303d/gitlab.com/traffic/load-balancing-analytics?lb=codesuggestions.gitlab.com&pool=all&time-window=360)

- [ ] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

No. The resources in the `unreview-poc-390200e5` project, as mentioned before, are not all on IaC and thus not labeled consistently. The resources managed by Runway are currently not labeled either.

### Operational Risk

_The items below will be reviewed by the Reliability team._

The biggest operational risk of the service is that the underlying infrastructure the service runs on (provided by the Runway platform) experiences an issue that we haven't seen before or haven't anticipated, and needs a change or update to accommodate unforeseen issues. This is because this is the first time the Runway platform will be used in a production environment.

AI Gateway on the current legacy infrastructure runs as pods inside of Kubernetes (GKE). In the new Runway based infrastructure, it runs in [Google Cloud Run](https://cloud.google.com/run) instead. There is another major operational risk is that performance and function of the service differs between these two platforms, leading to unexpected results.

If the AI Gateway experiences a failure, it could potentially impact all AI features in GitLab.com. The specific impact would depend on the nature of the failure and how each feature interacts with the AI Gateway. The AI Gateway contains pre- and post-processing logic for AI features that can also affect the availability of the service. Additionally, the AI Gateway handles requests from self-managed instances to provide feature parity with SaaS (see <https://gitlab.com/groups/gitlab-org/-/epics/10516>), and thus the AI features on those instances can also be affected by failures. This shared infrastructure approach between self-managed and GitLab.com is also novel in our architecture, and introduces new operational challenges of possible interference between the two actors. See <https://gitlab.com/gitlab-org/gitlab/-/issues/420123> for a related discussion.

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

The link to the ai-gateway in the metrics catalog is [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/ai-gateway.jsonnet). Metrics are visible on [this dashboard](https://dashboards.gitlab.net/d/ai-gateway-main/ai-gateway-overview?orgId=1)

### Deployment

_The items below will be reviewed by the Delivery team._

Deployment of ai-gateway is done via the [Runway Platform](https://about.gitlab.com/handbook/engineering/infrastructure/platforms/tools/runway/). This is a completely automated
and self-container deployment methodology that does require any involvement from the delivery team.

The change management issue for partly enabling this feature is at https://gitlab.com/gitlab-com/gl-infra/production/-/issues/16262.

The runway platform has redimentary support for rollbacks in case of a bad deployment, however "roll forward" (`git revert` bad code and deploy) is the preferred approach.

Artifacts built for this feature are the responsibility of the developers of ai-gateway. In this case, it is done in the [pipeline of the project itself](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/blob/main/ci/gitlab-com.yml?ref_type=heads)

### Security Considerations

_The items below will be reviewed by the Infrasec team._

* GCP Project: `gitlab-runway-staging` and `gitlab-runway-production` for the Cloud Run services, `unreview-poc-390200e5` for the legacy GKE cluster, and the Vertex AI API usage for both the GKE and Runway deployments.
* New Subnets: No new subnets at this stage
* VPC/Network Peering: No VPC peering at this stage. The connection between the Runway GCP projects and the `unreview-poc-390200e5` project was done by giving
  the AI Gateway Runway service account permissions as `Vertex AI user` on the latter. This was done by manual SRE action, as IAM permissions on this project are not
  currently managed as Infrastructure as Code. See <https://console.cloud.google.com/iam-admin/iam?project=unreview-poc-390200e5>
* DNS names: https://codesuggestions.staging.gitlab.com https://codesuggestions.gitlab.com https://ai-gateway.staging.runway.gitlab.net https://ai-gateway.runway.gitlab.net
Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  * Public IPs for the above dns endpoints
  * Load Balancers for the *.runway.gitlab.net domains. These load balancers existing in the `gitlab-runway-staging` and `gitlab-runway-production` GCP projects
* Other (anything relevant that might be worth mention): This feature is deployed and run on top of the Runway Platform. The infrastructure code for how this works lives in the [provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) (responsible for onboarding new projects with permissions needed), and then via ci-jobs and infrastructure code in [ci-tasks](https://gitlab.com/gitlab-com/gl-infra/platform/runway/ci-tasks)

### Secure Software Development Life Cycle (SSDLC)

_The items below will be reviewed by the Infrasec team._

All cloud infrastructure resources are not currently labeled, though there is an issue to [perform this work](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/68)

Infrastructure updates are mostly automated by the use of the [Renovate Bot](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.md) deployed in `gl-infra`

We use terraform for all IaC in the environments.

We have checkov covering the terraform repositories [here](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/.gitlab-ci.yml?ref_type=heads#L13-17) and [here](https://gitlab.com/gitlab-com/gl-infra/platform/runway/ci-tasks/-/blob/main/.gitlab-ci.yml?ref_type=heads#L25-29)

There are three separate terraform states involved in the infrastructure management of this feature.

1. In the Runway provisioner [here](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/terraform)
1. For the staging environment, it is [here](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments/ai-gateway/-/terraform)
1. For the production environment, it is [here](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments/ai-gateway/-/terraform)

There are no secrets stored in the terraform state, all secrets are stored in Google Secrets Manager

Currently a [distroless base image is not in use](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/blob/main/Dockerfile?ref_type=heads#L1)

Currently there is no security scanning covering these containers

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

The AI gateway authorizes requests by validating JSON Web Tokens transmitted from GitLab instances using public-key cryptography.

The AI Gateway does not add any new forms of data storage.

The AI Gateway talks to Gitlab via the standard public api address (https://gitlab.com/api/v4) and all traffic is encrypted. Communication between users and the AI Gateway is done via https, with certificates managed by Cloudflare. Cloudflare is in front of codesuggestions.gitlab.com via [a load balancer](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/24649cabcf51fe3a6c0c2b325c0c2de9c6a80685/environments/gprd/cloudflare-codesuggestions.tf) providing DDoS protection and a WAF if needed. The CloudFlare load balancer is backed by two pools:

* The Runway deployment pool, itself behind [a GCP load balancer, with certificates handled via Google certificates manager](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/2d2dd657f4720fecba760504e933141e6c11b869/reconciler/loadbalancer.tf#L1-43)
* The legacy GKE deployment pool, fronted by [an ingress NGINX](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/blob/24763231ee70cb778a9668249dc6fe8df1aacada/manifests/ingress/ingress-nginx.yaml) with [certificates handled via LetsEncrypt](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/blob/61c0850e4da3214e9409672b741d167023b1fd5c/manifests/cert-manager/cluster-issuer.yaml)

### Logging & Audit

_The items below will be reviewed by the Infrasec team._

Sensitive customer data is not visible in logging. Logs are being kept in our standard logging infrastructure [here](https://log.gprd.gitlab.net/app/r/s/hgxv3) and we are keeping appropriate access and audit logs.

### Compliance

_The items below will be reviewed by the Infrasec team._

N/A

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

Logs available [here](https://log.gprd.gitlab.net/app/r/s/hgxv3)

Dashboards available [here](https://dashboards.gitlab.net/d/ai-gateway-main/ai-gateway-overview?orgId=1)

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

There is no data stored as part of ai-gateway and thus no backup/restore/dr/retention requirements

### Deployment

_The items below will be reviewed by the Delivery team._

Deployment of ai-gateway is done via the [Runway Platform](https://about.gitlab.com/handbook/engineering/infrastructure/platforms/tools/runway/). This is a completely automated
and self-container deployment methodology that does require any involvement from the delivery team.

The change management issue for partly enabling this feature is at https://gitlab.com/gitlab-com/gl-infra/production/-/issues/16262.

This feature does not have any version compatibility requirements with other components.

This feature is covered by our QA blackbox tests https://gitlab.com/gitlab-org/gitlab/-/blob/master/qa/qa/specs/features/ee/api/14_model_ops/code_suggestions_spec.rb#L55

The runway platform has redimentary support for rollbacks in case of a bad deployment, however "roll forward" (`git revert` bad code and deploy) is the preferred approach. Rollbacks are done by going to a previous deployment pipeline at https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/pipelines and re-running the "🚀 Production Deploy 100%" job.

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- There's a [runbook for the AI Gateway](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ai-gateway/README.md) for service-specific troubleshooting.
- There's also a [runbook for Runway](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/runway/README.md) with general troubleshooting regarding deployments
- The service is sending alerts linked to runbooks, currently with severity `s4`. Example [here](https://gitlab.slack.com/archives/C0586SBDZU2/p1695051615926879).
- The dashboards for the AI Gateway deployed through Runway is <https://dashboards.gitlab.net/d/ai-gateway-main/ai-gateway-overview?orgId=1>.


### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

The rollout will be performed through [this Change Management issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/16361). We list there several metrics,
including latency apdex scores and error rates, whose degradation would make us trigger a rollback. The change to traffic rules is applied through ops.gitlab.net
pipelines. After that, the deployment process of new version of the service through Runway is done entirely on [gitlab.com](https://gitlab.com).
