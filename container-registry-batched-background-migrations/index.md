# Container Registry: Batched Background Migrations (BBM)

The new batched background migration feature is thoroughly documented in the corresponding specification available [here](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md), which was first introduced in [gitlab-org/container-registry!1614](https://gitlab.com/gitlab-org/container-registry/-/merge_requests/1614) with collaboration from Database team members.

The feature is technically ready to proceed to staging and production, following the rollout plan described [here](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md#rollout-plan), starting with "Phase 1".

## Experiment

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the all of the fields are populated.

  The service catalog entry is located [here](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/services/service-catalog.yml?page=2#L1762).

### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
   
   Yes, we do rely on IaC for this service and feature. This is done in the [Kubernetes Workload configurations for GitLab.com](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/).

- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

   No, the service is not currently behind Cloudflare. For context, the latest related change is [here](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/16468).

- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

  N/A. No new infrastructure is added. No infrastructure changes needed. The only changes are application settings.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [x] List the top three operational risks when this feature goes live.

  - Background migration consumes system/database resources, leading to elevated latency and/or error rate for API component.

  - Faulty logic in the background migration process can corrupt migrating tables/columns.

  - Inability to run background migrations due to failure or network connectivity issues accessing database.

  _NOTE_: The registry team enforces [performance testing and monitoring](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md#performance-testing-guide) of newly introduced background migrations against a recent production database clone during the [container registry MR review process](https://gitlab.com/gitlab-org/container-registry/-/blob/master/.gitlab/merge_request_templates/Default.md?ref_type=heads) when adding new background migrations.

- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

  Batched Background Migrations (BBMs) are designed to minimize risk to the Registry by running as background processes that do not directly interfere with the operation of other Registry components, such as its API (used for pushing, pulling, or deleting images). 

  For the **database**, potential failures such as lock contention, long-running queries, or database resource exhaustion could affect migration target tables, leading to performance degradation for other Registry operations against those tables. To mitigate this, BBMs use batch processing with `min_value` and `max_value` range to break down tasks into smaller, incremental chunks. This approach avoids long-running locks and ensures efficient query execution without overloading the system.

  The distributed nature of the Registry introduces the risk of multiple instances running BBM jobs in parallel, which could result in duplicate jobs and wasted resources. This is addressed by synchronizing job execution (using a distributed lock), ensuring that only one instance runs a background migration job at any given time.

  For the **BBM worker process**, excessive resource consumption (such as CPU or memory) could compete with critical Registry processes, impacting overall system stability. To reduce this risk, only one worker process is run per Registry instance, with background migration loads distributed across multiple instances via randomized job scheduling. Additionally, [CLI](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md#cli) tools allow infrastructure administrators to pause or resume BBMs during periods of high load, giving them full control over resource allocation.

  In terms of migration logic, [automatic retries](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md#retry-and-recovery) are implemented to handle transient failures, up to a configurable threshold, allowing the migration process to recover from temporary issues without interruption. Idempotent job logic ensures that retries produce consistent results without side effects, preserving data integrity. Additionally, each job execution is time-scoped to prevent runaway processes, ensuring migrations complete within a reasonable time frame and without negatively impacting the Registry's performance.

  Overall, BBMs are heavily isolated from core Registry operations, meaning that failures within the background migration system has limited likelihood in affecting essential registry tasks. Infrastructure administrators have [the necessary tools to pause, resume, or retry BBMs](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/registry/background-migrations.md?ref_type=heads#managing-background-migrations-on-gitlabcom), ensuring that migrations can be managed and controlled.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

  - Registry: [link](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/metrics-catalog/services/registry.jsonnet);
  - PgBouncer: [link](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/metrics-catalog/services/pgbouncer-registry.jsonnet);
  - Patroni: [link](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/metrics-catalog/services/patroni-registry.jsonnet)

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  Yes: [link](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/19020).

- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

  Can be safely rolled back. To do so, we need to revert the [Kubernetes Workloads](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com) MR where the `registry.database.backgroundMigrations.enabled` configuration was set to `true` (defaults to `false`).

- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

  CNG.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects:
  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  - Other (anything relevant that might be worth mention):

  No new resources added.

- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

  Yes.

- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.

  No.

- [x] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

  Yes. We use the renovate bot for dependencies of the registry application. The OS and container image dependencies are managed and updated by Distribution in the CNG project.

- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

  No. No new IaC introduced.

- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

  We are not creating new containers. The registry image is built in the CNG "pipeline".

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

  No.

- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

  N/A.

- [x] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?

  Yes. See network policy [here](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/46f3215f556f32d8ea29e1a3e95bcd107cae3a7f/releases/gitlab/values/values.yaml.gotmpl#L334).

- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

  No.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?

  N/A. No new customer specific data in this feature.

- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

  - Database: Full path of each container repository, which includes the GitLab namespace/group(s) and projects name. The remaining data pertains to container images, namely their manifest and runtime configuration payloads. 
  
  The new data that is stored in the database pertains to [the background migration and job metadata](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md#structure).
 
  - Storage (GCS): Container image layers. No changes in this feature.

- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard/) (customer data is RED)?

  Repository metadata is classified as ORANGE, the remaining registry data is classified YELLOW.

  The new background migration data is classified YELLOW as well.

- [x] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

  Yes, for both the GCS bucket ([source](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets/-/blob/95d5617488ed2165cdb99d29395e26f13aa89c5a/bucket_registry.tf#L17)) and the database (`auth.log` and PgBouncer logs). All logs are sent to ElasticSearch, then archived in GCS.

  No related changes in this feature.

  - [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
  - [ ] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

  N/A

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to examples of logs on https://logs.gitlab.net

  A list of log entries and Kibana filters can be found in the feature runbook [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/registry/background-migrations.md).
 
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

  - [`registry: Overview`](https://dashboards.gitlab.net/d/registry-main/registry3a-overview)
  - [`registry: Database Detail`](https://dashboards.gitlab.net/d/registry-database/registry-database-detail)

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [x] Are there custom backup/restore requirements?

  No. We are relying on the existing infrastructure. Established backup/restore procedures unchanged.

- [x] Are backups monitored?

  Yes ([source](https://dashboards.gitlab.net/goto/h0VxFX4Ng?orgId=1)).

- [x] Was a restore from backup tested?

  The database backup restore tests have been verified as working [here](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2056).

- [x] Link to information about growth rate of stored data.

  The growth rate of the background migrations and jobs tables is primarily driven by the number of migrations and jobs being executed over time. As new migrations are added, the `batched_background_migrations` table will grow, with each migration creating multiple jobs in the `batched_background_migration_jobs` table. The number of jobs is determined by the size of the migration (`batch_size`) and the range of data being migrated.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  Yes: [link](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/19020).

- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?

  No.

- [x] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?

  No direct QA tests exist/are feasible.

- [x] Will it be possible to roll back this feature? If so explain how it will be possible.

  Can be safely rolled back. To do so, we need to revert the [Kubernetes Workloads](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com) MR where the `registry.database.backgroundMigrations.enabled` configuration was set to `true` (defaults to `false`).


### Security

_The items below will be reviewed by the InfraSec team._

- [x] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?

  No foreseeable new attack vectors introduced by this feature.

- [x] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

  None.

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
