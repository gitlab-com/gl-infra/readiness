# SAST in the IDE

The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Experiment

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the all of the fields are populated.

  Runway integrates with [service catalog](https://docs.runway.gitlab.com/reference/observability/). <https://gitlab.com/gitlab-com/runbooks/-/blob/2050aaeb672b546e25eb9aecc9672199f499827a/services/service-catalog.yml?page=3#L2699-2720>

### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?

  Runway uses IaC for [provisioning](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) and [deploying](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/tree/main/reconciler?ref_type=heads) infrastructure.
  - Our Runway deployment [documentation](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/runway.md) and [configuration](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/tree/main/.runway?ref_type=heads) is done in the [integration project](https://gitlab.com/gitlab-org/secure/sast-ide-integration). The service images are built in the [`sast-scanner-service` project](https://gitlab.com/gitlab-org/secure/sast-scanner-service).

- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

  Runway services fronted by External Load Balancers are protected against DDoS attack by GCP Cloud Armor. Runway services fronted by Cloud Connector are protected against DDoS attack by Cloudflare.

- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

  Runway requires [metadata](https://schemas.runway.gitlab.com/RunwayService/#metadata) to automatically label all cloud infrastructure resources.
### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [x] List the top three operational risks when this feature goes live.

  - Given the modest deployment [configuration](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/.runway/runway.yml?ref_type=heads#L18-19) and [defaults](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/ff691a91b970f8cefea58941d901799ea6a8b8e6/schemas/service-manifest/v1/manifest.schema.json#L340-369) for Runway services, we don't expect any operational risk outside of the feature itself.

  - The biggest operational risk to the feature is unexpected scanner behavior. Although we can correlate performance with input size, it is possible that certain source code inputs consume disproportionate resources.

  - It is also possible that the concurrency limits are too generous, and too many simulatenous scans on the same container degrades performance.

  - We expect the [rate limits](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloud_connector?ref_type=heads#rate-limiting) set in MR <https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/9322> are generous.

- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

  - Should the cloud service backend peformance degrade or fail completely, only IDE and direct API users would be affected.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

  Runway integrates with [metrics catalog](https://docs.runway.gitlab.com/reference/observability/). <https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/sast-scanner-service.jsonnet>

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

   No.

- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

  Runway uses continuous deployments and supports deploying reverted MRs. Runway automatically [rolls back](https://docs.runway.gitlab.com/guides/deployment-strategy/#auto-rollback) deployments experiencing elevated error rates.
  - The API endpoint availability cannot be disabled, although it has been discussed for a future revision.

- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).
  
  - Docker images are built in the [`sast-scanner-service` pipeline](https://gitlab.com/gitlab-org/secure/sast-scanner-service) and deployed via Runway in the [`sast-ide-integration` pipeline](https://gitlab.com/gitlab-org/secure/sast-ide-integration).

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects:

    - `gitlab-runway-staging`
    - `gitlab-runway-production`

  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
    - <https://cloud.staging.gitlab.com/sast/scan> / <https://cloud.gitlab.com/sast/scan>
  - Other (anything relevant that might be worth mention):

    - The Runway service is configured with `spec.network_policies.cloudflare: true` to limit access from Cloudflare IPs only, see https://runway-docs-4jdf82.runway.gitlab.net/workloads/services/#cloudflare.

- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

  - Yes.

- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.

  - Yes. <https://gitlab.com/gitlab-com/gl-security/product-security/appsec/appsec-reviews/-/issues/232>

- [ ] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

  - Not yet, see <https://gitlab.com/gitlab-org/gitlab/-/issues/499727>.

- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

  Runway uses checkov for [provisioning](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/.tool-versions) and [deploying](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/main/.tool-versions?ref_type=heads) infrastructure.

- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

  - Yes, `sast-scanner-service` uses the template `Container-Scanning.gitlab-ci.yml`.

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

  - The scanner service authenticates the `/scan` endpoint with JWTs generated by Cloud Connector using OIDC with OpenID providers `gitlab.com` and `customers.gitlab.com`. Only valid tokens with the correct unit primitive (scope) for our service are accepted.

- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

  - Yes.

- [x] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?

  Runway supports [restricting traffic](https://schemas.runway.gitlab.com/RunwayService/#spec_network_policies_cloudflare) for services fronted by Cloud Connector.
- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

  Runway services fronted by Cloud Connector are covered by Cloudflare WAF.
  
### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?

  - Yes. On scanner error it is possible that fragments of user source code are captured to ephemeral GCP logs.

- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

  - Only the built scanner image including [`sast-rules`](https://gitlab.com/gitlab-org/security-products/sast-rules). Customer data does not persist, the service is stateless.

- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard/) (customer data is RED)?

  - GREEN

- [x] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

  - N/A, no data is stored.

  - [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.

    - N/A

  - [x] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

    - N/A

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to examples of logs on <https://logs.gitlab.net>
- [ ] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there custom backup/restore requirements?
- [ ] Are backups monitored?
- [ ] Was a restore from backup tested?
- [ ] Link to information about growth rate of stored data.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
- [ ] Will it be possible to roll back this feature? If so explain how it will be possible.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [x] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
