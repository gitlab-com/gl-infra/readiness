# Grafana Mimir for Global Metrics View

## Mimir vs Thanos

Our present metrics environment uses Thanos as a global view across our multiple Prometheus servers.  Over the past 18 months, we've had increasingly large numbers of incidents related to Thanos and Prometheus issues, which initially spawned an epic to [change the way we monitor and report availability for GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1012) followed by an epic to [create an accurate, scalable metrics environment for high cardinality metrics](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1107).

Mimir is a tool created by Grafana to solve the same problem as Thanos -- providing a global viewpoint into metrics across multiple Prometheus instances and allowing Prometheus to scale to larger sets of data.

Architecturally, it is very similar.  The main differences are that it is a push based model using remote write rather than a pull based model, and that it provides multi-tenancy and sharding by default.  

![Mimir Architecture Overview](mimir-architecture-overview.png)

More details on the decision to move forward with Mimir can be found [in this comment](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1164#note_1642084746).

### Push vs pull

In Thanos, data is stored in a long term storage bucket and a short term TSDB on each Prometheus host, which holds the last 2 hours worth of data.  Every query in Thanos (from dashboards, human adhoc queries, and recording rules) must get its data from both the storage bucket and a thanos-sidecar process running on each Prometheus host.  The thanos-sidecar processes have had significant performance issues, timeouts, and crashes.

In Mimir, all data is remote written to a long term storage bucket, and the queriers get their data from there.  This removes a significant source of fragility in our metrics environment.

### Recording rule refactor

The effort to roll out Mimir has come with a number of less obvious advantages.  Today, our recording rules run in two different environments.  First, Prometheus [rules]((https://gitlab.com/gitlab-com/runbooks/-/tree/master/rules?ref_type=heads)) are evaluated across all Prometheus instances.  This meant that any new recording rule at that level was deployed to all Prometheus instances, whether or not their data was relevant.  Next, the global [Thanos rules](https://gitlab.com/gitlab-com/runbooks/-/tree/master/thanos-rules?ref_type=heads) run on top of those intermediate Prometheus rules.  This split system created a lot of confusion as it was not obvious how to trace a single recording rule down to the raw metrics that it was built from.

Mimir uses Prometheus Agent which does not support recording rules running in Prometheus.  We have migrated all the recording rules into [a single directory](https://gitlab.com/gitlab-com/runbooks/-/tree/master/mimir-rules?ref_type=heads), divided by environment/tenant and service.  This allows us to use Grafana's built-in tools to expand recording rules and makes it much easier to browse.

As part of the significant effort to refactor the recording rules, we also set up services to be tenant-aware, created the ability to scope them by environment and cluster to allow for rules to run more easily, and audited and scoped all of the handcrafted rules that have built up over the years.  That effort can be tracked in [this sub-epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1233).

The crafting of those recording rules is also now well understood by the entire newly formed Scalability:Observability team and will allow us to continue to improve the functionality of these systems moving forward.

### Chef vs labels

Another unseen advantage of Mimir vs Thanos is that it no longer depends on chef.  Thanos's entire relabeling system is based on often nested chef roles ([example](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/blob/master/roles/gprd-base-db-redis-sidekiq-catchall-a.json?ref_type=heads#L126-131)) to correctly label the metrics in prometheus.  With Mimir, we've moved the labels for the VMs to use standardized labels on the GCP instances themselves which are managed through IaC.

## Experiment

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [ ] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here:
  - Link to or provide a high-level summary of this new product feature.
  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.
  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.
  - List individuals are the subject matter experts and know the most about this feature.
  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.
  - List the member(s) of the team who built the feature will be on-call for the launch.
  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.

The Mimir service catalogue entry can be found [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/services/service-catalog.yml?ref_type=heads#L2495).

The Mimir service is owned by the [Scalability group](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/) and the [Observability team](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/observability/).

Their production and engineering managers and directors are
* [Liam McAndrew](https://handbook.gitlab.com/handbook/company/team/#liam-m), Engineering Manager
* [Rachel Nienaber](https://handbook.gitlab.com/handbook/company/team/#rachel-nienaber), Senior Engineering Manager
* [Marin Jankovski](https://about.gitlab.com/company/team/#marin), Director of Infrastructure, Platform

The individuals who know most about this feature are
* [Stephanie Jackson](https://gitlab.com/stejacks-gitlab)
* [Nick Duff](https://gitlab.com/nduff)
* [Gregarious Marco](https://gitlab.com/marcogreg)
* [Hercules Merscher](https://gitlab.com/hmerscher)

The teams/individuals who will be responsible for the reliability of this feature are
* The [Scalability:Observability team](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/observability/).


### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?

Yes.

- [ ] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

No.  This is similar to how Thanos works today.

- [ ] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

Yes.

Mimir is deployed through a [helm chart](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/mimir?ref_type=heads) to the ops k8s cluster.  The observability tenants for Mimir are configured in https://gitlab.com/gitlab-com/gl-infra/charts/-/tree/main/gitlab/observability-tenants?ref_type=heads.

We are using Prometheus Agent to scrape resources across all of our environments.  The k8s agents are deployed as part of [30-gitlab-monitoring](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/30-gitlab-monitoring/values-instances)

VMs are scraped using a set of specific labels (`gitlab_com_service` and `gitlab_com_type`) in [`config-mgmt`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/).  [Here is an example of those labels for Gitaly](https://gitlab.com/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/gprd/variables.tf?ref_type=heads#L966-982).  All of these labels are now set in IaC rather than done by nested rewrite code in chef.

It uses [Prometheus Agent](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/prometheus-agent?ref_type=heads) to scrape resources in VMs.  The helm chart for that and all of the configuration can be found in [the `prometheus-agent` chart](https://gitlab.com/gitlab-com/gl-infra/charts/-/tree/main/gitlab/prometheus-agent?ref_type=heads).


### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] List the top three operational risks when this feature goes live.


The top three operational risks would be inaccurate metrics data, false alarm pages to the EOC, or loss of metrics data.  The blast radius is fairly broad since this is metrics for all environments.  These are all issues that we are currently dealing with in our current Thanos metrics environment.

- [ ] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

We've mitigated those risks as best we can by having Mimir and Thanos run side by side so that we can audit and verify the data from one environment to the other, and will fail over to Mimir (with an easy failback to Thanos) and leave Mimir running as our main source of metrics for some time before we decommission Thanos.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

Mimir's metrics catalogue can be [found here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/mimir.jsonnet?ref_type=heads).

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

Multiple different services use Metrics for different purposes, and each of them will have a different rollback strategy.  As we're rolling this out next to Thanos, it will be easy to roll back to Thanos.  Each service will have a CR associated with it and the overall list can be found [in this issue](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2848).

- [ ] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

Yes, each CR will include rollback information to move the primary source of metrics back to Thanos.

- [ ] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

This is an infrastructure change and artifacts are handled in standard infrastructure fashion.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [ ] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects: N/A
  - New Subnets: N/A
  - VPC/Network Peering: N/A
  - DNS names:  `mimir-internal.ops.gke.gitlab.net`, `mimir-internal.ops-gitlab-gke.us-east1.gitlab-ops.gke.gitlab.net`
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):  Mimir is already exposed "internally" via a private service connect. This is done through an istio VirtualService object in kubernetes.
  - Other (anything relevant that might be worth mention):
- [ ] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
Yes.
- [ ] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
No.
- [ ] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?
Renovate bot is used for the [image tag](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/mimir/values.yaml.gotmpl#L2) and [helm chart](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/bases/environments/ops.yaml#L122).
- [ ] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

Yes, this is the same IaC methodology as we have today.

- [ ] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

N/A


### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [ ] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)? 

Yes, basic nginx authentication.

- [ ] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

Yes, and this is an improvement on Thanos today, as we have the ability to limit queries to specific tenants, thus allowing only access to the data that they need.

- [ ] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)? 

Yes.

- [ ] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

Mimir uses an Nginx Gateway and Basic Authentication for login, similar to Thanos (which uses Google oauth).  We have plans to add [rate limiting and DDoS protection to Mimir in the future](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/3478).

Presently, it is a DNS entry in Cloudflare similar to how Thanos works today.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [ ] Did we make an effort to redact customer data from logs?
N/A
- [ ] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
Metrics data for GitLab.com
- [ ] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?
Yellow
- [ ] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
No, similar to what we have today.
 - [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
Logs are availabile for 7 days similar to our standard today.
 - [ ] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.
 N/A

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to examples of logs on https://logs.gitlab.net

Mimir logs are in the same location as the Thanos logs:  <https://nonprod-log.gitlab.net/app/r/s/BLV6G>

- [ ] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

There are a multitude of dashboards for it.  The SLI dashboard is [here](https://dashboards.gitlab.net/d/mimir-main/mimir3a-overview?orgId=1).  The Mimir overview dashboard is [here](https://dashboards.gitlab.net/d/ffcd83628d7d4b5a03d1cafd159e6c9c/mimir-overview?orgId=1&refresh=5m) and provides links to most of the others.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there custom backup/restore requirements?
N/A
- [ ] Are backups monitored?
N/A
- [ ] Was a restore from backup tested?
N/A
- [ ] Link to information about growth rate of stored data.

The growth rate of stored data is similar to the environment we have today, as are the backup/restore requirement.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

Multiple different services use Metrics for different purposes, and each of them will have a different rollback strategy.  As we're rolling this out next to Thanos, it will be easy to roll back to Thanos.  Each service will have a CR associated with it and the overall list can be found [in this issue](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2848).

- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
N/A
- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
N/A
- [ ] Will it be possible to roll back this feature? If so explain how it will be possible.

Each service can be rolled back to point the main source of their metrics data to Thanos.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the troubleshooting runbooks.

Mimir's runbooks can be found [here](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/mimir?ref_type=heads).

- [ ] Link to an example of an alert and a corresponding runbook.
We will turn on SLI based alerting similar to all the rest of our environment before this goes live.
- [ ] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

All SREs have access to the Mimir environment as it runs in our standard GKE environments.

Querying for Mimir can be done through the [Explore section](https://dashboards.gitlab.net/goto/y4s8rmYSR?orgId=1) of Grafana.  We have created a tenant for each environment but there is also a 'global' tenant that includes all four Gitlab environments (`gprd`, `gstg`, `ops`, `pre`).

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.

[Test prometheus agent HA failover with Mimir](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2842)

- [ ] What are the potential scalability or performance issues that may result with this change?

This is a new system that we're using, and with any new system, there are tuning and scalability changes that we've made.  The main issue is that this may require more human intervention at the beginning to additionally tune various sections of the environment. We're also aware that Mimir is presently susceptible to the same 'poison query' issue that Thanos suffers from today where a very poor performant query can cause issues to the metrics and recording rules.  We are waiting for an upstream change to be merged and then can roll out a [separate read path](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/3386) for Mimir Rulers which will mitigate this issue.

- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?

Growth of the metrics stack is always a concern, however, we will be in a better position to expand with Mimir than we are with Thanos.

- [ ] Are there any single points of failure in the design? If so list them here.

No, the Mimir architecture is highly available.

- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

Running the two environments side by side mitigates almost all the failure scenarios.  The worst case scenario is potentially that Mimir has a significant issue and Thanos continues its failures, however, this is quite unlikely as Mimir has been running side by side with Thanos since November of 2023.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?

The Mimir data is stored in three GCS metrics buckets:  metrics, ruler, and alertmanager.  We have moved these to be [US multi-regional buckets for DR purposes](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2843).  We have also turned on soft-delete which would allow us to restore accidentally deleted data.

- [ ] How does data age? Can data over a certain age be deleted?

Presently, we are using Google's [Soft Delete](https://cloud.google.com/storage/docs/soft-delete) feature, which allows for data to be restored if accidentally deleted.  Data is soft deleted for 7 days (allowing restore) and then permanently deleted after 7 days.  We may wish to eventually age out metrics data, but presently, we keep it permanently.

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).

All of the efforts for performance validation are part of [this epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1234).  The issues that are specifically associated with performance are:

- [Performance test and validate Mimir](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2853)
- [Add Mimir HPA support for components that are compatible](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2840)
- [Test prometheus agent HA failover with Mimir](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2842)

- [ ] Link to any load testing plans and results.

[Add Mimir HPA support for components that are compatible](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2840)

- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
N/A
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
N/A
- [ ] Are there retry and back-off strategies for external dependencies?
N/A
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?
N/A

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

Multiple different services use Metrics for different purposes, and each of them will have a different rollback strategy.  As we're rolling this out next to Thanos, it will be easy to roll back to Thanos.  Each service will have a CR associated with it and the overall list can be found [in this issue](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2848).

- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?

Yes, Mimir has the same SLIs as any standard service in our environment.

- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?

N/A
