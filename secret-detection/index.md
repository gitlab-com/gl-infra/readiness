# Secret Detection service readiness

## Overview

Secret Detection service is a simple stateless service responsible for detecting secret leaks in the given payload. The service is intended to be used by the [Secret Push Protection(SPP)](https://docs.gitlab.com/ee/user/application_security/secret_detection/secret_push_protection/) feature in the near future.

The service exposes only gRPC endpoints as of now and the deployment is managed by [Runway](https://docs.runway.gitlab.com/). The service is available at https://secret-detection.runway.gitlab.net. The service is not publicly accessible and is solely accessible by Rails monolith where the SPP feature resides. So, the service is deployed only in the regions wherever Rails monolith is deployed i.e., `us-east1` region.

On a high level, the SPP feature uses the service differently depending on the service's maturity level:

- **Experiment**: In this stage, the SPP feature does not actively use the service for triggering secret detection scans. It instead fires a copy of scan request to the service _asynchronously_ without waiting for its result. This is done to record missing metrics received on the service side in the background. The service disruption will not impact the feature usage. More details available [here](https://gitlab.com/gitlab-org/gitlab/-/issues/494910).

- **Beta**: The SPP feature will use the service actively for triggering secret detection scans, however, this action will be wrapped behind a feature flag to tackle any unexpected service disruptions. In order to keep the customer experience seamless, the SPP feature will also keep an alternative method (gem-based approach) as a fallback incase of service unavailibility. More details availble [here](https://gitlab.com/gitlab-org/gitlab/-/issues/499249).

- **General Availibility**: In this level, the primary expectation is for the service to be capable of handling traffic that are recorded in the previous levels. However, we will continue using feature flag in the event of service disruptions.

## Documentation

- [Service Overview](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_detection/#phase-2---standalone-secret-detection-service)
- [Service Architecture](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_detection/decisions/004_secret_detection_scanner_service/)
- [Choosing Runway as deployment solution](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_detection/decisions/005_use_runway_for_deployment/)
- [Source code](https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service)

## Experiment

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [X] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the all of the fields are populated.

    Service catalog entry is available [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/services/service-catalog.yml#L2677)

### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [X] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?

    The service's IaC is managed by Runway in [provisoner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) and [reconciler](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl) projects.

- [X] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

    Runway manages the service within a [GCP internal load-balancer](https://console.cloud.google.com/net-services/loadbalancing/details/crossRegionalInternalHttp/secret-detection-internal-url-map?project=gitlab-runway-production).

- [X] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

    Runway handles the resource labelling based on the [runway configuration](https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service/-/blob/5988998b657726ef5afa289924faf4c6f437d8d9/.runway/runway.yml) for the service which reflects in the [GCP Cloud Run instance](https://console.cloud.google.com/run/detail/us-east1/secret-detection/yaml/view?project=gitlab-runway-production).


### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [X] List the top three operational risks when this feature goes live.

    - Ability to handle high traffic with large request payload sizes (2MB+). This could shoot up memory consumption of the service.
    - Since the service deployment is done at a single region, disruption in the service availability will impact .com GitLab Ultimate customers across the globe.
    - In case of a failure in GCP Cloud Run, the service becomes unavailable or degraded. In case of a failure in Runway control, the ability to deploy new versions will be impacted.

- [X] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

    Service failures would not directly impact the customers since we have [plans](https://gitlab.com/gitlab-org/gitlab/-/issues/499249) to switch over to alternate methods in the feature during service failures.

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [X] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

    Metrics Catalog for the service is configured [here](https://gitlab.com/gitlab-com/runbooks/-/blob/9ddc395e8ac92bebf74ca3eefc77f13efd372ad7/metrics-catalog/services/secret-detection.jsonnet)

### Deployment

_The items below will be reviewed by the Delivery team._

- [X] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

    N/A
- [X] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

    As mentioned in the Overview section, the service unavailibility will not impact the feature usage in the Experiment maturity level. However, for the further maturity levels, we have [plan](https://gitlab.com/gitlab-org/gitlab/-/issues/499249) to introduce a feature flag to disable the calls to the service. 
    
- [X] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

    A Docker image is built in the service project's [CI pipeline](https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service/-/blob/main/ci/templates/build.yml?ref_type=heads#L4) for each commit event. That image is hosted in the project's [GitLab Container Registry](https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service/container_registry/6452962) and its URI provided to Runway for the deployment.


### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [X] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects:
  - New Subnets:
  - VPC/Network Peering:
  - DNS names: `secret-detection.runway.gitlab.net`
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...): None (Internal LB)
  - Other (anything relevant that might be worth mention):

- [X] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
    
    Yes. Secure coding guidelines were following for this feature. We've also integrated SAST scanning to identify any vulnerabilities in the development stage.

- [X] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
    
    Application security best practices were followed, however, yet to be validated/approved by an Application Security Review. There's an [issue](https://gitlab.com/gitlab-com/gl-security/product-security/appsec/appsec-reviews/-/issues/238) to track the review.

- [X] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

    Currently, this process is being done manually.

- [X] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.
    The service does not own any IaC files in the project. 

- [X] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

    We are [using GitLab Container Scanning](https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service/-/blob/46dbe6cc4b7ee0aa6d01f0652c106be3e49d729e/ci/templates/test.yml#L8) for the container image that is built in the CI.

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [X] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

    We have added a basic static token-based form of authentication as an extra security layer despite the service not being public. The client has to embed a token in the request which is matched against a static token (securely read from Hashicorp vault) in the service. Read more about it [here](https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service#calling-grpc-endpoints-from-terminal).

- [X] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

    N/A

- [X] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?

    N/A

- [X] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

    WAF is [configured](https://gitlab.com/gitlab-com/gl-infra/config-mgmt/-/blob/cd69cae7ac10f916a056d2069a4ab41eeed25f2e/environments/ops/cloudflare-rate-limits-waf-and-rules.tf) for the `*.gitlab.net` domain under which the secret-detection service is deployed.


### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?

    The service does not log customer data in the logs.

- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

    The service being stateless does not store any data in the system except error information in the logs.

- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard/) (customer data is RED)?

    The data is rated GREEN since the service does not store the kind of data that exposes GitLab or its customers to any harm or material impact.

- [x] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

    Audit logs are available in [Cloud Audit Logs](https://cloud.google.com/logging/docs/audit).

  - [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.

    We are leveraging the default 30 day retention for the production Runway project in GCP.

  - [x] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

    N/A

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to examples of logs on https://logs.gitlab.net
- [ ] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there custom backup/restore requirements?
- [ ] Are backups monitored?
- [ ] Was a restore from backup tested?
- [ ] Link to information about growth rate of stored data.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
- [ ] Will it be possible to roll back this feature? If so explain how it will be possible.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?

