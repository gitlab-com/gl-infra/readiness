# Container Registry: Database Load Balancing

The new database load balancing feature is throughly documented in the corresponding specification available [here](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-load-balancing.md), which was first introduced in [gitlab-org/container-registry!1628](https://gitlab.com/gitlab-org/container-registry/-/merge_requests/1628) with collaboration from Database, Delivery and Database Reliability team members.

The feature has been enabled in Staging and we are now technically ready to proceed to production, following the rollout plan described [here](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-load-balancing.md#rollout-plan), starting with "Phase 1".

## Experiment

### Service Catalog

_The items below will be reviewed by Scalability:Practices team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the all of the fields are populated.

  The service catalog entry is located [here](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/services/service-catalog.yml?page=2#L1762).

### Infrastructure

_The items below will be reviewed by the Scalability:Practices team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?

  Yes, we do rely on IaC for this service and feature. This is done in the [Kubernetes Workload configurations for GitLab.com](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/).

- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

  No, the service is not currently behind Cloudflare. For context, the latest related change is [here](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/16468).

- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

  N/A. No new infrastructure is added. No infrastructure changes needed. We're relying on existing standby replica hosts (unused so far) and the only changes are application settings.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [x] List the top three operational risks when this feature goes live.

  - Consul, PgBouncer and/or Patroni failure or network connectivity issues, leading to the inability to detect and refresh replicas on the application side;

  - K8s registry pod crash loop due to unexpected application startup issues. This happened in [staging](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/25918) and the underlying cause was fixed and validated, but worth mentioning still;

  - Elevated latency and/or error rate for API component due to unforeseen bugs. Showing outdated data to users due to high replication lag.

- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

  As explained [here](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-load-balancing.md#rollout-plan), the rollout plan for the first iteration (Phase 1) will only affect a single API endpoint. This endpoint is the lowest in terms of risk and criticality (unrelated to pulling, pushing or deleting images) and has a low request rate.

  In terms of components and dependencies, in the worst case scenario, the primary instance will continue serving all the traffic, as it is doing now.

  As for features to reduce risk, the registry is able to continue working in the absence of available/reachable replicas. We have seen this in staging when the network connectivity between the registry and Consul was not properly setup. More details [here](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/25918).

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

  - Registry: [link](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/metrics-catalog/services/registry.jsonnet);
  - PgBouncer: [link](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/metrics-catalog/services/pgbouncer-registry.jsonnet);
  - Patroni: [link](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/metrics-catalog/services/patroni-registry.jsonnet)
  - Redis: [link](https://gitlab.com/gitlab-com/runbooks/-/blob/a57074d9785b7046ce5776ee65165aa10aab948c/metrics-catalog/services/redis-registry-cache.jsonnet)

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  Yes: [link](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18977).

- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

  Can be safely rolled back. To do so, we need to revert the [Kubernetes Workloads](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com) MR where the `registry.database.loadBalancing.enabled` configuration was set to `true` (defaults to `false`).

- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

  Yes, it's build in CNG, by the [`gitlab-container-registry`](https://gitlab.com/gitlab-org/build/CNG/-/blob/bdd3704ea8233cc4bc3a06b7753b22732fb350ea/.gitlab/ci/common.gitlab-ci.yml#L523) job, which produces the [`registry.gitlab.com/gitlab-org/build/cng/gitlab-container-registry:vX.Y.Z-gitlab`](https://gitlab.com/gitlab-org/build/CNG/container_registry/741707) image.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects:
  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  - Other (anything relevant that might be worth mention):

  No new resources added. We will be making use of the already existing standby replicas and infrastructure.

- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

  Yes.

- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.

  No.

- [x] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

  Yes. We use the renovate bot for dependencies of the registry application. The OS and container image dependencies are managed and updated by Distribution in the CNG project.

- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

  No. No new IaC introduced.

- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

  We are not creating new containers. The registry image is built in the CNG "pipeline".

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

  No.

- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

  N/A.

- [x] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?

  Yes. See network policy [here](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/46f3215f556f32d8ea29e1a3e95bcd107cae3a7f/releases/gitlab/values/values.yaml.gotmpl#L334).

- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

  No.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?

  N/A. No new data in this feature.

- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

  - Database: Full path of each container repository, which includes the GitLab namespace/group(s) and projects name. The remaining data pertains to container images, namely their manifest and runtime configuration payloads. No changes in this feature.
  - Storage (GCS): Container image layers. No changes in this feature.

- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard/) (customer data is RED)?

  Repository metadata is classified as ORANGE, the remaining is classified YELLOW.

  No new data in this feature.

- [x] Do we have audit logs for when data is accessed? If you are unsure or if using the central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

  Yes, for both the GCS bucket ([source](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets/-/blob/95d5617488ed2165cdb99d29395e26f13aa89c5a/bucket_registry.tf#L17)) and the database (`auth.log` and PgBouncer logs). All logs are sent to ElasticSearch, then archived in GCS.

  No related changes in this feature.

  - [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
  - [-] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [x] Link to examples of logs on https://logs.gitlab.net

  A list of log entries and Kibana filters can be found in the feature runbook [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/registry/db-load-balancing.md).
 
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

  - [`registry: Overview`](https://dashboards.gitlab.net/d/registry-main/registry3a-overview)
  - [`registry: Database Detail`](https://dashboards.gitlab.net/d/registry-database/registry-database-detail)
  - [`patroni-registry: Overview`](https://dashboards.gitlab.net/d/patroni-registry-main/patroni-registry3a-overview)
  - [`pgbouncer-registry: Overview`](https://dashboards.gitlab.net/d/pgbouncer-registry-main/pgbouncer-registry3a-overview)
  - [`consul: Overview`](https://dashboards.gitlab.net/d/consul-main/consul3a-overview)

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [x] Are there custom backup/restore requirements?

  No. We are relying on the existing infrastructure. Established backup/restore procedures unchanged.

- [x] Are backups monitored?

  Yes ([source](https://dashboards.gitlab.net/goto/h0VxFX4Ng?orgId=1)).

- [x] Was a restore from backup tested?

  The database backup restore tests have been verified as working [here](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2056).

- [x] Link to information about growth rate of stored data.

  N/A. No new data in this feature.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  Yes: [link](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18977).

- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?

  No.

- [x] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?

  No direct QA tests exist/are feasible. Feature currently enabled in staging.

- [x] Will it be possible to roll back this feature? If so explain how it will be possible.

  Can be safely rolled back. To do so, we need to revert the [Kubernetes Workloads](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com) MR where the `registry.database.loadBalancing.enabled` configuration was set to `true` (defaults to `false`).

### Security

_The items below will be reviewed by the InfraSec team._

- [x] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?

  No foreseable new attack vectors introduced by this feature.

- [x] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

  None.

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

### Operational Risk

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Scalability:Practices team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
