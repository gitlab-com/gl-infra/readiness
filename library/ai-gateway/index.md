# AI-gateway

This document has been moved to an architecture blueprint. See the
[AI-gateway architecture](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/ai_gateway),
the initial discussion happened in
[!166](https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/166)
in this repository, but was moved to a blueprint in
[gitlab-org/gitlab!126484](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/126484).
