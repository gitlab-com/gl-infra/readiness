The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Overview

Wiz Runtime Sensor is the `DaemonSet` that was already deployed on the `Pre` environment ([MR](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/3252)) to monitor malicious actions that run on container environments. Wiz Runtime Sensor internal document is added [here](https://internal.gitlab.com/handbook/security/infrastructure_security/tooling/wiz-sensor/).

The end goal is to deploy the Wiz agent in every Kubernetes cluster that would follow the definition of [What is considered Production?](https://gitlab.com/gitlab-com/gl-security/security-assurance/team-commercial-compliance/compliance/-/blob/master/production_definition.md#what-is-considered-production). This includes `staging`, `ops` and `gprd` environments.

## Experiment

### Service Catalog

- [ ] ~Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here~:
  - ~Link to or provide a high-level summary of this new product feature.~
  - ~Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.~
  - ~List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.~
  - ~List individuals are the subject matter experts and know the most about this feature.~
  - ~List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.~
  - ~List the member(s) of the team who built the feature will be on-call for the launch.~
  - ~List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.~ 

    N/A - This is not a GitLab feature and does not directly connect with any GitLab Services. This agent/daemon is being deployed on the Infrastructure to collect the signals/events and categorize the events as safe/malicious.
    
    But an overview of the architecture can be found in the [Internal Doc](https://internal.gitlab.com/handbook/security/infrastructure_security/tooling/wiz-sensor/#architecture).
### Infrastructure

_The items below will be reviewed by the Reliability team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?  
      
      It will be deployed with IaC using a Helm chart provided by Wiz. Currently, it is deployed in the `Pre` environment. This [Merge Request](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/3252) can be reviewed for the IaC code.
      
- [ ] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?  

      Wiz Runtime Sensor is the agent that is deployed on the Kubernetes Clusters and is not a public facing service. As it is not a public facing service and not expose any public facing endpoints, it would not require us to add the DDoS solutions.

- [ ] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?  

      Not sure about adding the labels on the Pods created by this sensor, but we can add it going forward with the deployment if it is required
      
### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] List the top three operational risks when this feature goes live.

      The Wiz Sensors are added as the `DaemonSet`, so it would add the sensor in all the nodes. It might consume upto 300 Mb of memory (which is rarely the case) and on the busy node or node with low resource availablility it might results into the resource shortage.

- [ ] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

  The sensor/agent failure should not create an operational risk, but as a result, no events/alerts are sent to the SIEM. That will severely limit our Security visibility and our ability to respond to Security Incidents.

  It is also relevant to consider that we are sending data to the Wiz Platform that might be regarded as RED data. For example, if someone runs a command that the Wiz Sensor considers potentially malicious, and that command exposes a secret, that will be sent to the Wiz Platform. If the Wiz Platform suffers from a security breach, that type of data can also be leaked.

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

      The Dashboards are created outside the metric catalog and below are the links.
      
      [Global Stats](https://dashboards.gitlab.net/d/f90b6258-20d3-4462-8b31-a26ab955ca26/wiz-sensor-global-stats?orgId=1) This dashboard gives us the global view of the top 2-5 CPU and Memory usage statistics of Pod/Cluster.

      [Cluster/Pod Stats](https://dashboards.gitlab.net/d/e122d5d2-a1cb-470f-b8af-86460528f865/wiz-sensor-cluster-stats?orgId=1) This dashboard allows us to review the specific pod-level CPU and Memory to isolate any issues with particular Pod/Cluster.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

      Not created yet

- [ ] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

      N/A as this is not a product feature but there are steps documented to [disable the sensor](https://internal.gitlab.com/handbook/security/infrastructure_security/tooling/wiz-sensor/#how-to-disable-wiz-sensor-from-the-environment)

- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

      N/A The images from the provider are directly referenced from the helm chart. Wiz helm charts are published at https://charts.wiz.io/, and the code can be found in this GitHub repository: https://github.com/wiz-sec/charts

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [ ] ~Link or list information for new resources of the following type:~
  - ~AWS Accounts/GCP Projects:~
  - ~New Subnets:~
  - ~VPC/Network Peering:~
  - ~DNS names:~
  - ~Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):~
  - ~Other (anything relevant that might be worth mention):~

  Only New Pods would be created in each node and it is internal pods which would not expose and public service. The Pods would require the access to Wiz Backend to send the results. Access requirements are documented and it can be found in the table under the section for [outbound communication](https://internal.gitlab.com/handbook/security/infrastructure_security/tooling/wiz-sensor/#outbound-communication)

- [ ] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

      N/A This is vendor tool and we do not have control on the source code

- [ ] ~Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.~

  We don't have access to the code since this is a third-party vendor. But there was a Third Party Risk Management review done by the Security Assurance team which can be found here: https://gitlab.com/gitlab-com/gl-security/security-assurance/security-risk-team/third-party-vendor-security-management/-/issues/1946
- [ ] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

      It would be deployed under the gitlab-helfiles repo, and that is covered by the Renovatebot, we have to add the necessary config which is added as part of the [MR](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/4475)
      
      The new versions of the chart are published at https://wiz-sec.github.io/charts/index.yaml. That should be enough to have renovated bumping in the chart. The chart has the container image tag set in the [values.yaml](https://github.com/wiz-sec/charts/blob/master/wiz-sensor/values.yaml#L120).

- [ ] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

      It would be deployed under the `gitlab-helmfiles` repo and sast scanning is not added to the repo.

- [ ] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

      Vendor image would be directly used to deploy the sensor, we are not creating the image using `Dockerfile`

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [ ] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

      N/A we are not adding any new forms of authentication. However the sensor would need user/password (generated by Wiz Portal) to connet to Wiz backend. Those are stored in vault and are referenced from vault.

- [ ] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

   The pods are assigned a Service Account with a ClusterRole that grants permissions to get, list, and watch pods, namespaces, nodes, daemonsets, replicasets, deployments, jobs, cronjobs, statefulsets, replicationcontrollers, and serviceaccounts across the entire cluster. Details can be found in the code here: https://github.com/wiz-sec/charts/blob/master/wiz-sensor/templates/clusterrole.yaml

  The container itself does not run as privileged. Still, it has a lot of Capabilities enabled, including the ability to move between namespaces, use eBPF, hash files, monitor network events, and potentially kill processes, running as a non-root user (2202) with a read-only root filesystem and specific SELinux options, depending on the cluster's configuration and version. Details can be found here: https://github.com/wiz-sec/charts/blob/master/wiz-sensor/templates/daemonset.yaml#L73

- [ ] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?

      No network policies are applied as of now in `Pre` environment, as it will only communicate with the Wiz backend to send the logs and Wiz registry (`wizio.azurecr.io`) to download the images, the policies can be added to restrict communication to specific domains/ports.

- [ ] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?
 
     Wiz Runtime Sensor is the agent that is deployed on the Kubernetes Clusters and is not a public facing service. As it is not a public facing service and not expose any public facing endpoints, it would not require us to add the DDoS/WAF solutions.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [ ] Did we make an effort to redact customer data from logs?

      N/A the service does not take any customer data.

- [ ] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
      
      Wiz Sensor would scan the system calls and the detections would only send metadata to Wiz Portal. The alerts are generated in Portal and would transitiont to SIEM, so there are no secretss stored / transition to Wiz Portal or the SIEM

- [ ] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?

      The data in the Wiz SaaS is Orange (vulnerability information), however Wiz runtime sensor would have direct access to view thecommands that are run in the environment so the scope of the data access should be considered Red. Also Wiz Sensor would only send metadata to the Wiz Portal.

- [ ] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
      
       N/A

 - [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.

       The logs are transfered to Wiz backend Portal and from the port the alerts are transitioned to Devo.

 - [ ] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to examples of logs on https://logs.gitlab.net
- [ ] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

    The Dashboards are created outside the metric catalog and below are the links.

    [Global Stats](https://dashboards.gitlab.net/d/f90b6258-20d3-4462-8b31-a26ab955ca26/wiz-sensor-global-stats?orgId=1) This dashboard gives us the global view of the top 2-5 CPU and Memory usage statistics of Pod/Cluster.
    
    [Cluster/Pod Stats](https://dashboards.gitlab.net/d/e122d5d2-a1cb-470f-b8af-86460528f865/wiz-sensor-cluster-stats?orgId=1) This dashboard allows us to review the specific pod-level CPU and Memory to isolate any issues with particular Pod/Cluster.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there custom backup/restore requirements?
- [ ] Are backups monitored?
- [ ] Was a restore from backup tested?
- [ ] Link to information about growth rate of stored data.
      
      N/A, this is vendor developed scanner that is being used to scan the realtime malicious actions so it is not applicable here.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
      
      We have created one for staging and deployed it on staging
      Artefacts: [staging change management issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17629) / [staging MR](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/4348)

- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?

      N/A

- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?

      N/A

- [ ] Will it be possible to roll back this feature? If so explain how it will be possible.

      N/A as this is not a product feature but there are steps documented to [disable the sensor](https://internal.gitlab.com/handbook/security/infrastructure_security/tooling/wiz-sensor/#how-to-disable-wiz-sensor-from-the-environment)

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

      These sensors mainly hook itself to eBFP probes and listen to the syscalls and send the alerts to the Wiz backend. The risk is if the malicious docker image or the binary is downloaded from the vendor then it might have some malicious binary on our environment.

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call Reliability SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

      There are not troubleshooting runbooks are we the sensor is vendor developed, but we have created the [alert for OOM kills](https://gitlab.com/gitlab-com/runbooks/-/merge_requests/6773/diffs) and it would be routed to the InfraSec team to understand and work with vendor on the over utilization or work with Infra to extend memory limits.


### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?

If the wiz-sensor accumulates many events and doesn't flush them, it could lead to OOM. This happened during our stress tests and was fixed by the Wiz team, but it is still something that we should pay attention to. This is still mitigated by the resource limits enforced by Kubernetes on the DaemonSet.

- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

     
### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

   N/A, the security alerts would be sent to Devo

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

      Performance tests are already done using the tools, there is [specific section](https://internal.gitlab.com/handbook/security/infrastructure_security/tooling/wiz-sensor/#performance-or-stress-test) on the Wiz Sensor Internal doc. The deployment has CPU and memory limits and hence we do not expect it much to create issues. 

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
      
      The Change Management Issue is not created yet but it would be used for deployment 

- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?

      Frequent of OOM kills should trigger us to rollback the sensor and work with vendor to understand if it requires more memory or are there any unknown bugs in the sensor

- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?

      N/A