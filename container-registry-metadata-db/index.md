# Container Registry Metadata Database

## Summary

The Package team has been working on a new version of the Container Registry that relies on a metadata database (DB) to enable [online Garbage Collection (GC)](https://gitlab.com/gitlab-org/container-registry/-/issues/199) ([specification](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/db/online-garbage-collection.md)), and [address several other challenges and feature requests](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/#challenges).

With the new version now implemented, we have been working on the rollout, starting with GitLab.com ([&5523](https://gitlab.com/groups/gitlab-org/-/epics/5523)). For this purpose, we have defined a gradual migration plan that was proposed, widely discussed, and approved in [gitlab-org/container-registry#374](https://gitlab.com/gitlab-org/container-registry/-/issues/374). Our current focus is [Phase 1](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-1-the-metadata-db-serves-new-repositories). A separate readiness review will be created for [Phase 2](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-2-migrate-existing-repositories), as that will require additional changes that are not yet implemented.

The vast majority of the information required for this document was already persisted in the [architecture blueprint](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database) and the [gradual migration plan proposal](https://gitlab.com/gitlab-org/container-registry/-/issues/374). We will refer to specific portions of these whenever necessary, but it is highly recommended to read both to understand the remaining of this document.

The metadata DB and online GC are both operational features, which means that they are hidden from users, and do not represent any change at the API level. Because of this, the only visibly impacted metric will be the response time of API calls, especially for reads, which account for [96%](https://thanos.gitlab.net/graph?g0.expr=(sum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20method%3D~%22put%7Cpatch%7Cpost%7Cdelete%22%2C%0A%20%20%20%20%20%20%20%20code%3D~%22%5E2.*%7C404%22%0A%20%20%20%20%20%20%7D%5B3d%5D%0A%20%20%20%20)%0A)%0A%2F%20%0Asum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20method!%3D%22options%22%2C%0A%20%20%20%20%20%20%20%20code%3D~%22%5E2.*%7C307%7C404%22%0A%20%20%20%20%20%20%7D%5B3d%5D%0A%20%20%20%20)%0A))%20*%20100&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20method%3D~%22head%7Cget%22%2C%0A%20%20%20%20%20%20%20%20code%3D~%22%5E2.*%7C307%7C404%22%0A%20%20%20%20%20%20%7D%5B3d%5D%0A%20%20%20%20)%0A)%0A%2F%20%0Asum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20method!%3D%22options%22%2C%0A%20%20%20%20%20%20%20%20code%3D~%22%5E2.*%7C307%7C404%22%0A%20%20%20%20%20%20%7D%5B3d%5D%0A%20%20%20%20)%0A))%20*%20100&g1.tab=1&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D) of all traffic. As the migration progresses and there are more and more repositories with their metadata stored on the DB, the response time for API calls is expected to drop considerably, as we do not have to reach out to the storage backend (GCS) to perform lookups. Additionally, with online GC, we can expect the storage space used on GCS to grow at a slower pace, as we continuously delete unused data. This will be visible once we start approaching the completion of the migration. Until then, the used space will grow due to the [dual "partition" strategy](https://gitlab.com/gitlab-org/container-registry/-/issues/374#storage-split).

## Architecture

Please refer to [this](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/#current-architecture) and [this](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/#new-architecture) section of the architecture blueprint for a detailed description of the current architecture (as-is, no metadata DB) and the new architecture (including availability concerns, failure scenarios, and their impact), respectively.

In the first iteration, we are not going to have database load balancing, which means that the registry will depend on the availability of the primary database node. We have planned to implement active database load balancing (routing reads to read-only replicas on the fly), but this requires a third dependency - Redis. This was proposed in [gitlab-org/container-registry#396](https://gitlab.com/gitlab-org/container-registry/-/issues/396), but the consensus was that the risk involved in introducing another depending at the same time that we are introducing the metadata DB was too high. The intention is to revisit this during the migration if we observe concerning scaling/availability issues (reactively) or between Phase 1 and Phase 2 (proactively). Please see the issue for additional details.

## Operational Risk Assessment

As we switch most of the read and write load from the storage backend (GCS) into the DB, its availability and performance become increasingly important. We have never operated a Container Registry with a DB, so we need to remain vigilant during the gradual rollout to spot and address any potential scaling or performance issues. These may require infrastructure changes, application improvements/bug fixes, or both.

The [architecture blueprint](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-2-migrate-existing-repositories) details how a DB failure might impact the Container Registry, Rails, and end-users. It is important to note that during the percentage base rollout of Phase 1, these impacts are limited to _new_ repositories _which_ have their metadata stored on the DB. In the event of a DB failure, existing repositories, or new repositories that are not considered eligible, will remain fully operational with GCS as their single hard dependency.

As previously mentioned, it was decided to leave a DB load balancing feature implementation to a later date. Similarly, the Container Registry does not have any rate-limiting at the moment. This is another use case for using Redis, and as such, it falls in the same category as the DB load balancing feature.

Once we enable the metadata database in production (with a [configuration](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/configuration.md#database) change on the Container Registry), the registry will be ready to store and serve metadata for _new_ container repositories using the metadata database. The remaining, i.e., the gradual rollout, will be controlled using Rails feature flags. New repositories will only be marked as eligible for being registered and served in/from the database when the Rails feature flags are enabled. All feature flags and their role in this process are detailed in [gitlab-org/gitlab#335260](https://gitlab.com/gitlab-org/gitlab/-/issues/335260).

We introduced a k8s job for running DB schema migrations during deployments if the registry version is changing. We tested and confirmed db schema migrations to be working in [gitlab-com/gl-infra/delivery#1890](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1890). New pods will always check if the actual DB schema is up-to-date with their current version before starting up, to ensure that db migrations always run before pods of a new replica set are starting.

We can pause/resume the rollout by toggling the `container_registry_migration_phase1` feature flag, but this will not automatically revert/redo the registration of the repositories that have already been stored in the database and the new bucket "partition" (see the next paragraph for a manual way to do so). Pausing the rollout will only avoid adding more new repositories to the database and the new bucket "partition", so it acts as a way to limit impact if we have to debug an ongoing issue. We will start with a 1% rollout (targetting ~1% of new repositories) and add many top-level namespaces to a deny list which will exclude them from the initial rollout, thus decreasing impact. The rationale and the numbers behind this strategy were described in the gradual migration plan and [gitlab-org/container-registry#408](https://gitlab.com/gitlab-org/container-registry/-/issues/408).

As detailed in the gradual migration plan ([here](https://gitlab.com/gitlab-org/container-registry/-/issues/374#temporarily-mirror-metadata-writes-to-new-storage-prefix)), we will keep writing metadata to the new bucket partition for the duration of Phase 1. This means that the metadata written to the database will also be written to the storage backend in a backward compatible format. The exception is for metadata (manifests, configurations, and tags) deleted by the online garbage collector, which only happens on the database. The single purpose of this metadata backup is to ensure that in the event of a disaster recovery scenario, we can (manually, with `gsutil`) copy metadata from the new to the old bucket partition to have a given repository (or all) being served by the old code path once again. Similarly, because deletes from the garbage collector are not applied to this backup metadata, we can also turn to it if we ever need to recover data that the garbage collector should not have deleted.

All possible failures, including their symptoms, impact, and possible solutions, were documented [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/registry/migration-failure-scenarios.md).

## Database

The Container Registry DB development adheres to the same [rules and principles](https://docs.gitlab.com/ee/development/database/) defined for the GitLab Rails database. Every change that touches the database requires a review and approval of the Database team.

The database schema, migrations, queries, and partitioning strategy were reviewed and optimized in strict collaboration with the Database team. The main issues where one can find more details about this are [gitlab-org/container-registry#104](https://gitlab.com/gitlab-org/container-registry/-/issues/104) and [gitlab-org/container-registry#228](https://gitlab.com/gitlab-org/container-registry/-/issues/228).

During the development process, we came up with an estimate for [rate](https://gitlab.com/gitlab-org/container-registry/-/issues/94) and [size](https://gitlab.com/gitlab-org/container-registry/-/issues/61) requirements for GitLab.com, and these were used to fine-tune the schema, partitioning strategy, and queries. We also made a 10x growth projection during the partitioning strategy review, which can be found [here](https://gitlab.com/gitlab-org/container-registry/-/merge_requests/566#gitlabcom-production-projections).

There is no concept of aged data on the Container Registry, but the new version includes an [online garbage collection](https://gitlab.com/gitlab-org/container-registry/-/blob/446d2df43c27ecfdcf288b07a58baded275f026d/docs-gitlab/db/online-garbage-collection.md) feature that takes care of deleting dangling artifacts both from the storage backend and the database.

## Security and Compliance

No new customer data is exposed in logs, neither new data (beyond what is already present in the storage backend) is saved on the metadata database. 

The most explicit piece of data stored on the database is the full path of each container repository, which includes the GitLab namespace/group(s) and projects name. The remaining data pertains to container images, namely their manifest (recipe) and runtime configuration payloads. All this data is already managed by the registry but only present on the storage backend. The database is not encrypted.

No new data is exposed at the API level. The API contract/functionality remain the same. There are no authentication or authorization changes either.

This version of the Container Registry requires a new piece of infrastructure - the PostgreSQL database cluster. The maintenance of this cluster follows the same practices applied to the GitLab Rails database. The cluster is set up using the same architecture and chef cookbooks as our main Patroni cluster.

Approval from Legal and Compliance for this change was obtained in [gitlab-com/legal-and-compliance#632](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/632/).

## Performance

The only related changes are at the database level. As described in [Database](#database), the schema, partitioning strategy, and queries were all validated and optimized in collaboration with the Database team. During this process, we extracted query plans for every single query against a test database inflated with artificial data (while respecting their usual form and distribution) and fine-tuned it. The latest iteration occurred in [gitlab-org/container-registry#566](https://gitlab.com/gitlab-org/container-registry/-/merge_requests/566).

The registry has no rate limiting/throttling right now, which remains as such with the addition of these new features. However, as some API read requests are expected to become significantly faster when using the database (namely listing tags, downloading manifests, and deleting tags and/or manifests), the faster response times may lead to additional requests being sent by clients. We will remain vigilant during the gradual rollout and consider rate limits (likely in Cloudflare for a first iteration) if necessary.

As described in the [architecture blueprint](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/#failure-scenarios), at the API level, there is no retry cadence when attempting to connect/communicate with the database. The registry will only try to establish a new connection when another request that requires database access is received. The underlying database driver will attempt to reconnect once in case of a temporary network error and bail if failed, at which point the registry will bubble up the error to the client.

At the online garbage collector level, as described in the [specification](https://gitlab.com/gitlab-org/container-registry/-/blob/446d2df43c27ecfdcf288b07a58baded275f026d/docs-gitlab/db/online-garbage-collection.md), the background workers (one for blobs another for manifests, in each registry instance) will handle errors gracefully when processing a task and try to postpone its subsequent review. In case of errors, tasks are not lost but instead left on the queue (regardless if it was possible to postpone it or not, which requires an `UPDATE` query). These queues are implemented with dedicated database tables.  Additionally, workers will apply an exponential backoff to delay their subsequent execution for every error that occurs. All of this is configurable and described in the [documentation](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/configuration.md#gc).

The design and implementation took into account the current GitLab.com rate requirements and followed best practices. However, being based on a new platform (database), the capacity to handle a brief spike of at least 2x the expected TPS can only be realistically assessed in production. No other environment comes even close to the amount of data or traffic observed there. For reference, the production registry has over [6 PiB](https://dashboards.gitlab.net/d/registry-storage/registry-storage-info?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-cluster=gprd-gitlab-gke&var-stage=main&var-namespace=gitlab&var-Deployment=gitlab-registry&viewPanel=3) and a rate of [+4.5k req/s](https://dashboards.gitlab.net/d/registry-main/registry-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&viewPanel=2271942982). The closest is staging, with over [500GiB](https://dashboards.gitlab.net/d/registry-storage/registry-storage-info?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-cluster=gstg-gitlab-gke&var-stage=main&var-namespace=gitlab&var-Deployment=gitlab-registry&viewPanel=3) and less than [10 req/s](https://dashboards.gitlab.net/d/registry-main/registry-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&viewPanel=2271942982).

## Backup and Restore

The database is subject to backups, including a delayed replica for situations where we may have to rollback in time to investigate and fix a data issue.

The storage backend needs no additional backups. We have [object versioning](https://cloud.google.com/storage/docs/object-versioning) turned on for the production GCS bucket. This is currently configured to keep up to [7 days](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/blob/master/environments/gprd/main.tf#L1885) of non-current (deleted) objects around, which means we can recover data from the storage backend if necessary as well. This retention threshold [will be increased to 30 days](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2039) before the rollout and will be kept as such during the whole migration. This gives us a bit more room to detect and recover from any unwanted deletion during online garbage collection.

The database backup restore tests have been verified as working in [gitlab-com/gl-infra/delivery#2056](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2056).

[Backups](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_success_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20type%3D%22patroni-v12-registry%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
and
[backup verifications](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_success_timestamp_seconds%7Bresource%3D%22https%3A%2F%2Fops.gitlab.net%2Fgitlab-com%2Fgl-infra%2Fgitlab-restore%2Fpostgres-gprd%20restore-verify%22%2Ctype%3D%22postgres-registry%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
are monitored based on emitted Prometheus metrics. Missing backup verification pipeline success notifications also are generating a [deadman's snitch alert](https://deadmanssnitch.com/snitches/1fdacaec5f), sending an email notification if there is no successful verification for longer than 1 day.

## Monitoring and Alerts

Registry logs are JSON formatted and available in ELK. No changes in this regard. Additional Prometheus metrics and Grafana dashboards were added, namely:

- Execution duration and count for every single query executed against the database, both at the API and online GC levels. Queries identified by a unique name. There is now a new database component, visible under the `database Service Level Indicator Detail` panel in the [`Container Registry / registry: Overview`](https://dashboards.gitlab.net/d/registry-main/registry-overview) dashboard. Apdex and request rate SLIs were [defined](https://gitlab.com/gitlab-com/runbooks/-/blob/8643092bbd5de9057d3bfa458881afb376edff12/metrics-catalog/services/registry.jsonnet#L96) for this component;

- Database connection pool (application side) metrics. These are shown in a new [`Container Registry / registry: Database Detail`](https://dashboards.gitlab.net/d/registry-database/registry-database-detail) dashboard;

- Various online GC metrics. These are shown in a new [`Container Registry / registry: Garbage Collection Detail`](https://dashboards.gitlab.net/d/registry-database/registry-database-detail) dashboard. There is also a new component for online GC, visible under the `garbagecollector Service Level Indicator Detail` panel in the [`Container Registry / registry: Overview`](https://dashboards.gitlab.net/d/registry-main/registry-overview) dashboard. Apdex, request rate and error rate SLIs were [defined](https://gitlab.com/gitlab-com/runbooks/-/blob/8643092bbd5de9057d3bfa458881afb376edff12/metrics-catalog/services/registry.jsonnet#L118) for this component;

- A new dashboard to keep track of useful GCS bucket metrics, namely size, object count, and throughput, is available at [`Container Registry / registry: Storage Detail`](https://dashboards.gitlab.net/d/registry-database/registry-database-detail). This is not related to any new functionality but was created to improve observability over storage.

There are no changes to the end-to-end customer experience. The API latency and error rate remain as the best indicator of these. The difference is that now the API latency and error rate can be impacted by the corresponding storage metrics and the ones from the database. The service SLA also remains unchanged.

Regarding alerts, besides the autogenerated ones for the new database and GC SLIs, two custom ones were created:

- Alert on [high number of overdue online GC tasks](https://gitlab.com/gitlab-com/runbooks/-/blob/master/rules/registry-gc-queues.yml). This allows us to be alerted in case the online GC workers are not processing pending tasks as expected, which can be due to a sudden surge in demand (that will likely resolve automatically), an application error, or the inability to consistently keep up with the demand with the current settings.

- Alert on [database connection pool saturation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/rules/registry-db.yml). This alert is for the application side connection pool and will be triggered if the average saturation across replicas goes above the configured threshold.

All custom alerts include links to relevant dashboards and troubleshooting runbooks.

## Responsibility

The main subject matter experts are @jdrpereira and @hswimelar from the Package team. @abrandl and @iroussos from the Database team were involved during the whole development process and have a solid understanding of the database schema, partitioning strategy, queries and how online GC works. 

@jdrpereira and @hswimelar will take responsibility for the reliability of the feature once it is in production. Both are available to be on call for the launch.

## Testing

The functionality was manually tested in pre-production ([gitlab-org&6425](https://gitlab.com/groups/gitlab-org/-/epics/6425)) and staging ([gitlab-org&6443](https://gitlab.com/groups/gitlab-org/-/epics/6443)). These covered the assessment of the API functionality (unchanged), data integrity, online GC behavior, and the migration procedure for Phase 1.

Tests also included simulation of some of the previously identified [failure scenarios](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/registry/migration-failure-scenarios.md). These were performed both in pre-production ([gitlab-org&6456](https://gitlab.com/groups/gitlab-org/-/epics/6456)) and staging ([gitlab-org&6684](https://gitlab.com/groups/gitlab-org/-/epics/6684)).

In regards to tests that run automatically in GitLab CI/CD pipelines, the Container Registry codebase includes a large number of unit and integration tests that run as part of the repository CI pipeline. These provide full coverage for the API (both with and without the metadata database enabled) and for online GC workers. All up/down migrations are also tested as part of the development pipelines to ensure no DDL/DML errors exist.

Apart from the Container Registry repository CI pipelines, QA tests validate the existing functionality and integration with GitLab Rails and the ability to push/pull images with Docker. These run in pre-production and staging. We also developed a specialized tool and QA tests for online GC. This only runs in pre-production as it requires customizing the registry configuration so that online GC reviews happen faster than by default. For a description of how these tests work, please see the [documentation](https://gitlab.com/gitlab-org/container-registry/-/blob/1eceeb6e48588f68154c6c0c5ce70e2d7ebc4a55/docs-gitlab/online-gc-tester.md).

Load testing in staging was performed as part of [gitlab-org&6684](https://gitlab.com/groups/gitlab-org/-/epics/6684). We have performed [write](https://gitlab.com/gitlab-org/gitlab/-/issues/340635) and [read](https://gitlab.com/gitlab-org/gitlab/-/issues/340634) tests. We went as high as simulating ~50% of the production request rate with no issues. The strategy, methodology and results are described in detail in the linked issues.

## Readiness reviewers

1. [@hswimelar](https://gitlab.com/hswimelar) (Development)
1. [@svistas](https://gitlab.com/svistas) (Quality)
1. [@vdesousa](https://gitlab.com/vdesousa) (AppSec)
1. [@hphilipps](https://gitlab.com/hphilipps) (SRE)
1. [@alejandro](https://gitlab.com/alejandro) (SRE)
