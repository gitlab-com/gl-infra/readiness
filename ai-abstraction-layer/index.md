# AI Abstraction Layer

The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Experiment

### Service Catalog

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here: The AI Abstraction Layer is not a separate service but a component in the monolith, and thus its SLIs and error metrics
are defined in the existing services where its functionality is executed. See "Monitoring and Alerting" bellow.
  - Link to or provide a high-level summary of this new product feature: The AI Abstraction Layer provides a uniform
  interface across 3rd party AI providers, and performs the required pre- and post- processing steps in the data pipeline
  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why: https://docs.gitlab.com/ee/development/ai_architecture.html
  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors:
    - Group: AI Frameworks
    - Engineering Managers: David O'Regan – @oregand
    - Product Managers: Torsten Linz – @tlinz
    - Directors:
  - List individuals are the subject matter experts and know the most about this feature: @gitlab-org/ai-powered/ai-framework/engineering/all
  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production: @gitlab-org/ai-powered/ai-framework/engineering/all
  - List the member(s) of the team who built the feature will be on call for the launch: @gitlab-org/ai-powered/ai-framework/engineering/all
  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency:
    - Anthropic, and Vertex AI APIs: features that depend on 3rd party providers would stop working if their respective provider
    suffers an outage. Users may se an error message when performing requests
    - Redis (as a dependecy via Sidekiq): `Llm::CompletionWorker` jobs can not be enqueued or executed in case of a redis outage

### Infrastructure

_The items below will be reviewed by the Reliability team._

This feature does not use specialized infrastructure, and runs on our pre-existing fleet of web, API, and Sidekiq servers.

- [ ] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered? N/A
- [ ] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)? N/A
- [ ] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines? N/A

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] List the top three operational risks when this feature goes live.
  - Since this feature relies heavily on external providers, hitting quota and/or rate limiting limits could degrade
it
- [ ] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?
  - If we exceed our quota or rate limits, the external providers will start responding with 429 errors. This could exceed our
  error rate thresholds and trigger alerts for the Sidekiq, Web or API services, depending on where the external provider call is
  made. Users would experience partial or total disruption of the related features. Errors on the `Llm::CompletionWorker` do not
  affect other workers. It is the responsibily of the feature implementers to gracefully handle situations in which the worker
  or any other component of the Abstraction Layer throws an error. We are not aware of any features using the Abstraction Layer
  that affect site-wide functionality in case of failure (for example, an error in the Duo Chat conversation box does not affect
  the rest of the page), but error handling gaps, present but undecteded, or future, could widen the impact of such failures.
  For GCP, we're already monitoring Vertex quota usage (see https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2430).
  For Anthropic rate limiting, see [their docs](https://docs.anthropic.com/claude/reference/errors-and-rate-limits#rate-limits).
  No quota monitoring option seems to be available.

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service: LLM-specific SLIs are included as part of the Sidekiq service ([src](https://gitlab.com/gitlab-com/runbooks/-/blob/52ce2057e978ad886790fad9ef3042ea7575ccb8/metrics-catalog/services/sidekiq.jsonnet#L124-130)): https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? I
f so, link to it here: No. The AI Abstraction Layer component is already deployed to production, and changes to it follow the same deployment
workflow as any other monolith changes.
- [ ] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag? As a result of [a feature flag consolidation
effort](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132347), the specific functionalities supported by the
Abstraction Layer are gated behind the fature flag `openai_experimentation`
([ref](https://gitlab.com/gitlab-org/gitlab/-/blob/528c91fcc1addd340beb8d6f680afc2bbc27bdf8/ee/app/workers/llm/completion_worker.rb#L26);
note that the FF name is no longer accurate, as multiple 3rd party providers are in use now). The FF can be disabled to
prevent access to AI features supported by the Abstraction Layer.
- [ ] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline): N/A

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [ ] Link or list information for new resources of the following type: N/A
  - AWS Accounts/GCP Projects:
  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  - Other (anything relevant that might be worth mention):
- [ ] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature? Yes
- [ ] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here: <https://gitlab.com/gitlab-com/gl-security/appsec/threat-models/-/issues/35>
- [ ] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date? N/A
- [ ] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why. N/A
- [ ] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities? N/A

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [ ] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

No. This features uses the existing authentication mechanisms in the GitLab monolith.

- [ ] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?:

The AI features have the same permissions and scope of the user performing the requests. Every time an AI feature
fetches a resource, it must validate that the requesting user has all the necessary permissions to access that resource,
and fail in case they fail any requirement. See [the guard on the
`CompletionWorker`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/workers/llm/completion_worker.rb#L34),
the
[`ExtraResourceFinder`](https://gitlab.com/gitlab-org/gitlab/-/blob/4d7aa53b01a872d88f7d83e3199caa12550b9820/ee/app/finders/llm/extra_resource_finder.rb#L42)
and [the LLM Authorizer
class](https://gitlab.com/gitlab-org/gitlab/-/blob/9a2f3eda8a3cfec52d85a8368bc8b459d1a13b06/ee/lib/gitlab/llm/chain/utils/authorizer.rb#L19).

- [ ] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)? N/A
- [ ] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)? N/A

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [ ] Did we make an effort to redact customer data from logs?

We purposely avoid user prompts in our logging messages. We establish application-wide filters for known sensitive
fields (for example, for Duo Chat questions
[here](https://gitlab.com/gitlab-org/gitlab/-/blob/e7227b457a13c7eb30776afd60e2593569fbd75e/config/application.rb#L233)).
To aid developers in debugging, we log prompt messages [here](https://gitlab.com/gitlab-org/gitlab/-/blob/19e000f59deb0313e29dff03387022012d9368ed/ee/lib/gitlab/llm/chain/concerns/ai_dependent.rb#L15)
if the log level is set to `DEUBUG` for the `llm` logger. That is controlled
[here](https://gitlab.com/gitlab-org/gitlab/-/blob/ccb0199d454ecc02ed6e10836a85f6c718b6d9fd/ee/lib/gitlab/llm/logger.rb#L11)
by the environment variable `LLM_DEBUG`. That variable **must not** be set to true in production environments.

- [ ] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

For some features, user-provided input that may include confidential or sensitive information is sent to 3rd party
providers to fulfil requests. Users must opt-in to be able to use such features, and are refered to our data usage docs
at <https://docs.gitlab.com/ee/user/ai_features.html#data-usage>.

User-provided input is not persisted in any of our datastores.

- [ ] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)? No data is persisted
- [ ] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template: Logs are sent to our pre-existing index patterns `pubsub-rails-inf-gprd-*` and `pubsub-sidekiq-inf-gprd-*`
- [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
roduction infrastructure: Since logs are sent to pre-existing pubsub topics, their current retention policies apply

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to examples of logs on https://logs.gitlab.net
  - [GraphQL chat operation logs](https://log.gprd.gitlab.net/app/r/s/5oA7D)
  - [`Llm::CompletionWorker` Sidekiq logs](https://log.gprd.gitlab.net/app/discover#/view/f411fc20-775c-11ee-a770-1d8ce4e214e2?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A60000)%2Ctime%3A(from%3Anow-1h%2Cto%3Anow)))
- [ ] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.
  - LLM Sidekiq metrics at <https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1> (under
  `SLI Detail: llm_completion`)

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

There is no data persisted for this feature

- [ ] Are there custom backup/restore requirements? N/A
- [ ] Are backups monitored? N/A
- [ ] Was a restore from backup tested? N/A
- [ ] Link to information about growth rate of stored data. N/A

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? I
f so, link to it here: No (see previous Deployment section)
- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments? No
- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)? No
- [ ] Will it be possible to roll back this feature? If so explain how it will be possible: Yes, using the `openai_experimentation` FF (see previous Deployment section)

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?

AI Features have in principle access to any resource in a GitLab instance. It is the responsibility of the AI
Abstraction Layer to validate each resource accessed against the requesting user's permission. At present, this needs to
be checked at various point across the Layer's code (see [Identity and Access Management](#identity-and-access-management)).
A gap in the code where a resource is fetched but not authorized could be used by an attacker to gain access to unauthorized resources. We have not found any
such gap in the current code.

- Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

There are no outstanding security-related epics & issues for this feature

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call engineers have access to this service.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
