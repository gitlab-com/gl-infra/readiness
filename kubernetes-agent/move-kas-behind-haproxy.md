# Move GitLab Agent for Kubernetes behind HAProxy

## Summary

This readiness review is to discuss the movement of the current GitLab Agent for
Kubernetes, which will be referred as KAS for the rest of this document, behind
our HAProxy Frontends.  This work encompasses two pieces of work to enable
better use of this service:

1. To enable certain features of KAS to be leveraged.
1. To enable a Canary stage

Currently, KAS was deployed with only a single (`main`) stage.  With the desire
to Auto-Deploy this service, having a `canary` stage is desired.  In order to
complete this work, we need some mechanism to provide us with appropriate
configuration to enable us to shift traffic over to the `canary` stage.  HAProxy
is already heavily leveraged for this.  We will reuse existing patterns to
satisfy this need.

KAS contains a capability to reverse proxy into a customers Kubernetes cluster
to run some commands that require access to Pods.  These include items such as
`kubectl cp` to copy files and `kubectl exec` for which we can execute anything
inside of a running container.  Currently these features do not work due to a
limitation of the GKE Ingress creating a GLB that does not support the SPDY HTTP
implementation.  With the move behind HAProxy, which does support SPDY, and
HAProxy being behind a TCP LB, we can now leverage these feature of KAS.  Please
see [Documenation Reference](#documentation-reference) below for links to KAS
documentation.

**Note that this readiness review will only mention the addition of the `canary`
stage as a bonus item.  The intention of this Readiness review is to discuss
the usecase of HAProxy being inserted between KAS and CloudFlare and not the use
of this stage itself.**

## Architecture

In the below charts, green lines indicate encrypted traffic.  Red lines indicate
unencrypted traffic.  During the transition, both of the below architectures
will be in place.  The routing of the request is governed by which DNS endpoint
the client request is being directed too.  No matter where we are in the
transition period, CloudFlare is still the first endpoint for client traffic.

### Before

The below represents a traffic flow that has been approved since the initial
implementation of this service.  A user/client leverages HTTPS to hit a Spectrum
app configured inside of CloudFlare.  CloudFlare simply reverse proxies this to
a Google Load Balancer of type HTTP that is configured via the Kubernetes
Ingress object installed via our Helm Chart.  TLS is terminated at the GLB and
unencrypted traffic flows between the GLB and the Pods that run KAS.

```mermaid
graph TD
    Z[user] --> |HTTPS port 443| A
    A[CloudFlare] --> |HTTPS port 443| B[GLB - type HTTP]
    B --> |HTTP port 8150| C[KAS]

    %% green == SSL traffic
    %% red   == non-SSL traffic
    linkStyle default stroke:green;
    linkStyle 2 stroke:red;
```

### After

A slightly more advanced configuration is in place.  CloudFlare is modified to
forward port 443 traffic to port 8150, where HAProxy then terminates SSL.
Unencrypted traffic then flows from HAProxy into our Kubernetes clusters to reach
the internal Service Endpoint that contains our KAS Pods.  We'll have 2
deployments, a `canary` and `main` stage which each have their own dedicated
services.  The location of where KAS is deployed remains the same and is
unchanged.

```mermaid
graph TD
    Z[user] --> |HTTPS port 443| A
    A[CloudFlare] --> |HTTPS port 8150| B[GLB - type TCP]
    B --> |HTTPS port 8150| C[HAProxy - mode http]
    C --> |canary| D[KAS canary stage]
    C --> |main| E[KAS main stage]

    %% green == SSL traffic
    %% red   == non-SSL traffic
    linkStyle default stroke:green;
    linkStyle 3,4 stroke:red;
```

With the above, we add a new failure endpoint, HAProxy.  We leverage HAProxy for
other services, so if this component were to fail, KAS would not be the only
negatively impacted service.  All other failure situations remain the same.

The location where TLS terminates is moved, however, KAS continues to receive
unencrypted traffic from the nearest network location.  The security stature of
this could be improved if desired.  KAS does support TLS encryption on the Pods.
This has not been tested as it's not currently leveraged as it stands.

This configuration opens up a new port, 8150, for which external traffic will
enter our front door.  This can be accessed directly hitting the port for which
the Google TCP load balancer or through CloudFlare at the KAS Fully Qualified
Domain Name, `kas.gitlab.com`.  CloudFlare forwards port 443 traffic to port
8150.  This is a new security situation that should be assessed.  HAProxy only
listens on port 8150 for this service, non SSL connections are rejected.  A
dedicated certificate was purchased via SSLMate and installed into our HAProxy
nodes.

The use of the GLB of type TCP enables us to fully leverage HAProxy to handle
the TLS termination and Layer 7 routing for the `canary` stage and handling of
various HTTP protocols, namely SPDY, which will be leveraged by Kubernetes and
KAS to make use of various features.

There are no singles points of failure introduced with this new configuration.
We still remain under a single regional failure if this were to happen.  But all
components have various levels of zonal redundancy.  Effectively no change in
this area of our tech stack.

Scaling of KAS will not change.  Our HPA manages this service, and the addition
of HAProxy will only add a set of healthchecks to the Pods.  These checks are
minor, very quick, and should not induce any abnormal amount of load onto the
service.  If it does, our existing HPA will exist to scale the service up as
currently designed.  HAProxy is horizontally scalable, though is not automatic.
If it is determined to be a point of conention, we can add additional frontend
nodes to tackle the traffic as necessary.  Currently, our expectation is that
we'll be able to leverage our existing `fe-\d\d-*` nodes, which intake all `ssh`
and Web Client traffic for GitLab.com.

## Operational Risk Assessment

### Performance

No performance issues are expected.  We are adding two additional network hops,
but all within the same VPC.  We can expect to see a few milliseconds of latency
added for all network traffic due to this.

### Scaling

As noted above, scaling our HAProxy workloads will be the key aspect of the
success of this project.  We'd like to rely on our existing HAProxy nodes, of
which we have 30 nodes at this moment.  Currently, we appear to spread at most,
1,300 connections per HAProxy node.  The addition of KAS, will add 6,000
connections to the entirely fleet.  Per node this is roughly 150 connections.

Reference data:

* [Connected Clients for
  KAS](https://dashboards.gitlab.net/d/kas-main/kas-overview?orgId=1&viewPanel=28)
* [HAProxy Frontend
  Connections](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum%20by%20(instance)%20(%0A%20%20rate(haproxy_frontend_connections_total%7Benv%3D%22gprd%22%2Cfrontend%3D~%22(api_rate_limit%7Ccheck_http%7Ccheck_https%7Ccheck_ssh%7Chttp%7Chttps%7Chttps_git_ci_gateway%7Cpages_http%7Cpages_https%7Cssh%7Cstats)%22%2Cinstance%3D~%22fe-%5B0-9%5D%5B0-9%5D-lb-gprd%5C%5C.c%5C%5C.gitlab-production%5C%5C.internal%3A9101%22%7D%5B1m%5D)%0A)&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

If HAProxy is unable to handle this additional load, we may need to horizontally
scale this fleet, which is currently a manual process.  We can monitor our
[Frontend Overview
Dashboard](https://dashboards.gitlab.net/d/frontend-main/frontend-overview?orgId=1)
for signs of poor performance during implementation.

There is existing work to go ahead and bolster these nodes, both in scaling
horizontally, which is recently completed, as well as instance type which is
currently scheduled.  Reference:

* https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16279
* https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16496

### Dependencies

HAProxy and an SSL certificate that is managed by [Certificate
Updater](https://gitlab.com/gitlab-com/gl-infra/certificates-updater) are the
only 2 added components to this configuration.  A failure of HAProxy is already
described above.  A failure to update our certificate automatically would result
in the inability to serve SSL connections error free.  Alerting exists when SSL
certificates are soon to expire.  The certificate is purchased via SSLMate, [a
well known system to the Reliability team
today.](https://gitlab.com/gitlab-com/runbooks/-/tree/master/certificates)  Note
that the use of emphemeral certificate systems was not leveraged due to the need
to install the certificates on each HAProxy node.  This is not something we have
an established pattern for.  We're following the established pattern already
leveraged by our front door today.

#### HAProxy Configuration

* When HAProxy is signaled to stop, it will wait upwards of 5 minutes before it
  forcibly quits.  It won't receive new connections during this period of time,
  but existing ones are expected to end.  Otherwise those connections will be
  forcibly disconnected when the HAProxy service hits the hard timeout.  Being
  that KAS operates via websockets, those connections will not have anything to
  tell them to end, thus for every HAProxy node, the connections will likely be
  terminated ungracefully.  KAS, however, should handle this gracefully and
  simply attempt a reconnect.
* HAProxy is configured with the following timeouts:
  * `timeout connect 5000` - if clients take longer than 5 seconds to complete
    creating a connection, kill it.
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20connect))
  * `timeout check 30000` - an additional check timeout that targets backends
    that is only taken into account when connections are established.  This is
    unrelated to clients talking to backends.
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20check))
  * `timeout client 90s` - if no activity from the client on an open connection
    for 90s passes, the connection is terminated.
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20client))
  * `timeout server 1h` - if the server is not responding to a client for 1
    hour, the connection is terminated
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20server))
  * `timeout tunnel 2h` - overwrites the `timeout server` and `timeout client`
    configurations when an HTTP request is upgraded.
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20tunnel))
  * `timeout client-fin 5s` - if a client connection was supposed to end but is
    not `ACK`'d, then close the connection
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20client-fin))
  * `timeout server-fin 5s` - same as the above but for the backend side instead
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20server-fin))
  * `timeout http-request 5s` - Max time allowed to wait for an HTTP request
    ([docs](http://docs.haproxy.org/1.8/configuration.html#4.2-timeout%20http-request))

### Risks

#### At Launch

* The change to the new infrastructure will induce a window of 60 seconds of
  potential instability/downtime. See [Rollback
  Capability](#rollback_capability) for additional details.
* HAProxy is unable to handle the additional load.
* New Port, `8150` is opened through our front door.

#### Continuous Operational Risk

* HAProxy is a slow system to horizontally scale.  This is already widely known
  and continues to be manually evaluated and managed today.  We already have
  appropriate alerting in place in the case that HAProxy begins to show signs of
  degradation.
* Inability to renew SSL Certificate.  We'll be alerted when SSL Certs are soon
  to expire and our Certificate Updater alerts us if a job fails to update
  certificates appropriately.

#### Rollback Capability

Until the transition is complete, both methods of accessing KAS will be
available.  This will ease the transition as either method of access KAS will
continue to work until a later time which the old method is cleaned up and
removed. Changing the configuration will be performed via a change to DNS,
specifically the destruction and creation of a new Spectrum app inside of
CloudFlare.  During this period of time, CloudFlare takes upwards of 60 seconds
to fully reconcile this change to their infrastructure.  It is expected that
during this window of time, new DNS queries to the KAS endpoint may fail, or may
point to the old endpoint.  The old endpoint will continue to receive traffic
and can safely operate without issue.  If a DNS request fails to resolve, the
KAS agent will later retry the request.  Existing websocket connections already
established will not be forcibly disconnected.  They will continue to operate
until the agent needs to reconnect.  This will happen upon our first deploy to
this service.  Deployments to this service are currently rare.

If problems are identified at launch, we can quickly revert our CloudFlare
configuration change.  This will suffer from the same problems noted above.
This feature is unable to be managed by a feature flag due to the nature of how
this configuration is taking place.

We will leverage a short maintenance window that will be defined in our Change
Request to complete this work.  The estimated time for this window will be
approximately 10 minutes, with a potential degradation of the service for about
1 minute.

#### Customer Interaction

Interaction of KAS is normally from a Kubernetes cluster with an agent which
reaches to the FQDN for KAS.  Nothing changes from this perspective.  Customers
currently go directly through CloudFlare for this traffic.  This will remain the
same post launch.

## Database

There is no change to how this service is configured as running.  Thus no
changes to how this service accesses internal systems.

## Security and Compliance

### Additions

* SSL Certificate used to be managed by Google, leveraging the Backend
  configuration which leverages GCP to generate a certificate for us.  This is
  being changed to leveraging SSLMate as noted above.
* Port `8150` is being opened up to receive traffic through our front door.

### SDLC

No changes to the way the KAS service being developed and deployed are changing
in this scenario.

The configurations that enable this new configuration are maintained inside of
our [config-mgmt](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt) repository.
We are using existing terraform modules to configure this new feature.  No new
terraform modules or code were created to manage this configuration.

The purchased SSL Certificate is stored leveraging GKMS as we do with all
certificates that are hosted on our FE nodes.  There is no change to this
behavior.

#### Access Management

There are no changes to how this service is managed or accessed by
administrators.

#### Data Storage

There are no changes to how this service manages the data or how it accesses
data in order for the service to operate.

#### Network Security

Port `8150` is being opened up on our Front Door.  Appropriate firewalls are in
place to ensure that this port can only be accessed by our HAProxy nodes.
Appropriate network policies are already in place for the acceptance of traffic
by the KAS deployments.  There are no changes to Egress network traffic for the
KAS service.

As this service remains behind CloudFlare, we are able to leverage existing
policies that apply to our domain.  This includes WAF and DDos protections.
With the addition of HAProxy, we can apply more targetted ACL's if deemed
necessary and compatible with the traffic if desired.

Reference Configurations:

* Terraform Firewall Rules on our FE nodes: https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/d3f9ff3abb60b8cfbe0b8a2c1d37e7385b24be3c/environments/gstg/variables.tf#L330
* Kubernetes Network Policies: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/6dbd2b87c00daaaf1ccb83da164810b9a9da43bc/releases/gitlab/values/values.yaml.gotmpl#L1218-1300

#### Logging

There are no changes to how logging is performed.

#### Compliance

No change to this service, how it operates, nor how it is deployed.  We should
be within compliance as we are following all existing patterns with this
configuration change that are common leveraged today.

## Performance

There are no changes to any rate limiting configuration that may be set by
either HAProxy and CloudFlare.  KAS agents installed in customer Kubernetes
clusters makes use of websockets and thus not really subject to rate limiting.
Please see the KAS documentation for workflow details.

No testing of performance/dependency impacts of any kind was performed.  The
nature of how the service operates is not changing and the service is already
well established in our infrastructure.

## Backup and Restore

There are no changes to the way data is managed for this service, this section
does not apply.

## Monitoring and Alerts

### Logging

There are no changes to the way logs are handled with this implementation.  We
are adding to the amount of logs that HAProxy will output due to it now handling
this traffic.  The way to retrieve and read these logs does not change.

### Observability

There are no changes to the metrics and observability for this service and
configuration.  All existing dashboards will and alerts will continue work as
intended.  No changes are made to any existing SLI's.  This service is well
established and does have metric definitions for SLI's.  Please refer to our
runbooks for this information.

## Documentation Reference

* KAS Architecture: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md
* KAS Request Routing: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/kas_request_routing.md
* CloudFlare Spectrum: https://developers.cloudflare.com/spectrum/
* HAProxy: https://www.haproxy.org/

## Responsibility

### SME's

#### Infrastructure

* Team Reliability - no change from current setup

#### GitLab Agent for Kubernetes

* Group Configure - no change from current setup

#### Launch

* Group Delivery - Team System - @skarbek

## Testing

The only testing performed was to validate functionality of KAS before and after
the configuration changes are made.  Prior to launch, we will have a test
cluster and test configuration that consists of the features desired to be
tested.  In this case, we'll want to validate that the agent deploys new
configurations and validate that various `kubectl` commands work.  A test repo
on staging, https://staging.gitlab.com/skarbek/test1, will be duplicated on
Production.  This repo contains a set of manifests as well as a CI configuration
that will be leveraged for test the features before and after the change.

Note that no additional performance/load/component/dependency testing was
performed.  This is due to this being an existing service that is well
established in our infrastructure.
