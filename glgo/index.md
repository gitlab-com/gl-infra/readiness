# glgo production readiness

## Overview

The service deployments are being **managed by [Runway](https://handbook.gitlab.com/handbook/engineering/infrastructure/platforms/tools/runway/)**. The service is available at https://auth.gcp.gitlab.com.

This service, named `glgo`, is being introduced as part of the [Google Cloud and GitLab S3C Integration](https://gitlab.com/groups/gitlab-org/-/epics/11412) project. As part of this project, we are adding support for integrations with Google Cloud services.

For authentication and authorization, we rely primarly on [Workload Identity Federation (WLIF)](https://cloud.google.com/iam/docs/workload-identity-federation). In this context, GitLab operates as the OpenID Connect (OIDC) provider and `glgo` is the identity service that mints the tokens that are later exchanged (through the [Security Token Service](https://cloud.google.com/iam/docs/reference/sts/rest)) for short lived access tokens used by the client tools (which includes the GitLab Rails backend) to interact with Google Cloud services.

Artifact Registry is the first Google Cloud service supported by this integration ([&11443](https://gitlab.com/groups/gitlab-org/-/epics/11443)). Other services will be supported, such as Cloud Deploy ([&11549](https://gitlab.com/groups/gitlab-org/-/epics/11549)).

The GitLab features that depend on `glgo` are those that rely on WLIF for authentication and authorization against Google Cloud services, which for now is the Google Artifact Registry integration ([&11443](https://gitlab.com/groups/gitlab-org/-/epics/11443)). This integration provides the ability to list, pull and push artifacts from/to an Artifact Registry repository. This functionality will be available through the GitLab UI but also through GitLab CI. The relevant user documentation will be introduced in [#425158](https://gitlab.com/gitlab-org/gitlab/-/issues/425158).

## Documentation

* Repository: https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo
* Design: https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/design-doc
* Technical: https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/tree/main/docs

## Experiment

### Service Catalog

_The items below will be reviewed by the Reliability team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here:
  - Link to or provide a high-level summary of this new product feature.
  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.
  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.
  - List individuals are the subject matter experts and know the most about this feature.
  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.
  - List the member(s) of the team who built the feature will be on-call for the launch.
  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.

The service is already in our service catalog, as shown [here](https://gitlab.com/gitlab-com/runbooks/-/blob/37e404413f856dc5bfe43441954215e4021a9e80/services/service-catalog.yml?page=3#L2561).

See [Overview](#overview) for a high-level summary and [Documentation](#documentation) for documentation links.

This service is currently maintained by the [Package](https://handbook.gitlab.com/handbook/engineering/development/ops/package/) group, and more precisely the [Container Registry](https://handbook.gitlab.com/handbook/engineering/development/ops/package/container-registry/) sub group.

The subject matter experts are [@grzesiek](https://gitlab.com/grzesiek) and [@jdrpereira](https://gitlab.com/jdrpereira).

[@jdrpereira](https://gitlab.com/jdrpereira) is available to be on-call for the launch.

This service depends on:
 - Cloud Run platform that it operates on (through Runway);
 - Vault. The private key used to generate tokens is stored in vault and loaded as a secret into Cloud Run ([Runway docs](https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/blob/master/secrets-management.md?ref_type=heads));
 - The ability to obtain the known issuer(s) JSON Web Key Set as described [here](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/main/docs/security.md?ref_type=heads#known-issuers).

The architecture design and decision record is being kept private ([here](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/design-doc)) for strategic reasons. We expect to make these details publicly available later [here](https://docs.gitlab.com/ee/architecture/blueprints/google_cloud_platform_integration).

### Infrastructure

_The items below will be reviewed by the Reliability team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
  
  IaC is managed by Runway in [provisoner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) and [reconciler](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl) projects.

- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

  Yes. Provided by Cloudflare.

- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

  Yes. Labelling managed by Runway.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [x] List the top three operational risks when this feature goes live.

- In case of a Cloud Run failure, the service is expected to become unavailable or degraded. In case of a Runway failure, the ability to deploy new versions is impacted;
- If the signing key secret goes missing, the application won't be able to start. There is currently no automated refresh mechanism ([glgo#45](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/issues/45), [glgo#46](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/issues/46));
- In case of a service failure, the features currently provided by the Google Cloud integration (see [Overview](#overview)) would become unavailable, and lead to an internal error message being presented to clients, as GitLab would fail to perform the token exchange.

- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

  There is no alternative to the functionality provided by this service. In the absense of the token exchange endpoint it provides, the features that depend on it (see [Overview](#overview)) would become unavailable.

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

  The metrics catalog entry is located [here](https://gitlab.com/gitlab-com/runbooks/-/blob/37e404413f856dc5bfe43441954215e4021a9e80/metrics-catalog/services/glgo.jsonnet).

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17666

- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

  The service is already available but all changes that depend/make use of it are behind a feature flag on the GitLab Rails side (https://gitlab.com/gitlab-org/gitlab/-/issues/443470) and that is going to be used to perform the rollout.

- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

  A Docker image is generated in the `glgo` project [CI pipeline](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/main/.gitlab-ci.yml?ref_type=heads) for each commit to the main branch. That image is hosted in the GitLab Container Registry and its URI provided to Runway for the deployment.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects: gitlab-runway-production / gitlab-runway-staging
  - New Subnets:
  - VPC/Network Peering:
  - DNS names: `auth.gcp.gitlab.com`; `glgo.runway.gitlab.net`
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  - Other (anything relevant that might be worth mention):
- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

  Application security best practices were followed and validated/approved by an Application Security Review.

- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.

  Yes. See https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/100#note_1785468918.

- [x] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

  There is a followup issue to automated the upgrade of dependencies ([glgo#35](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/issues/35)).

- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

  IaC managed by Runway. Uses checkov.

- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

  Container scanning is being enabled with [glgo!105](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/merge_requests/105).

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

  The service provides an OIDC endpoint for token exchange as described [here](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/c4de2f4e1c6b8930e942e3ad07eaea088dfe496b/docs/api.md#token-exchange).

- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

  Yes. Security best practices were followed and validated/approved by the Application Security Review.

- [x] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?
- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

  Yes. The configuration can be found [here](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/1638defc5fb28c2c9862ec7829c8bd83bf9fd38e/environments/gprd/cloudflare-rate-limits-waf-and-rules.tf). There is the WAF configuration for the `*.gitlab.com` domain. Then for the `*.gitlab.net` domain, that configuration, which may differ, can be found [here](https://gitlab.com/gitlab-com/gl-infra/config-mgmt/-/blob/1638defc5fb28c2c9862ec7829c8bd83bf9fd38e/environments/ops/cloudflare-rate-limits-waf-and-rules.tf).

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?

  The application emits logs for any internal error encountered during the requests processing. Additionally, we log the details on the input and output tokens for the [token exchange](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/c4de2f4e1c6b8930e942e3ad07eaea088dfe496b/docs/api.md#token-exchange) endpoint. This includes the token payload information documented [here](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html#token-payload).

  Logs are available in [Google Cloud](https://cloudlogging.app.goo.gl/wKEN2MeiFvPQhom58).

- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

  The private key used to sign tokens is stored in Vault and injected as a secret into Cloud Run. There is no other stored data.

- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?

  There is ORANGE classified data in application logs. Namely, customer metadata such as usernames and project paths, all of which are part of the [token payload](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html#token-payload) data. 

- [x] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

  Audit logs are available in [Cloud Audit Logs](https://cloud.google.com/logging/docs/audit).

 - [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
   We are leveraging the default 30 day retention for the production Runway project in GCP.
 - [x] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.
   N/A. No RED data classification.

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to examples of logs on https://logs.gitlab.net

  [Sample log](https://cloudlogging.app.goo.gl/up8MGK1MG3Fp4ykZA) filtered by correlation ID that shows entries for a token exchange request.

- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

  The default Runway dashboard is available [here](https://dashboards.gitlab.net/d/runway-service/runway3a-runway-service-metrics?var-service=glgo&orgId=1&var-PROMETHEUS_DS=PA258B30F88C30650&var-environment=gprd). The service overview dashboard is available [here](https://dashboards.gitlab.net/d/glgo-main/glgo3a-overview?orgId=1).

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [x] Are there custom backup/restore requirements?
- [x] Are backups monitored?
- [x] Was a restore from backup tested?
- [x] Link to information about growth rate of stored data.

This is a stateless service. The only persistent piece of data is the private signing key that is stored in Vault and injected into Cloud Run by Runway. The production Vault setup has a backup/restore procedure in place ([docs](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/vault.md?ref_type=heads#backing-up-and-restoring-vault)).

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  Yes: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17666

- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?

  This service is a new dependency for GitLab Rails. It is deployed independently through Runway. We pay attention to backward and forward compatibility when deploying new changes. We have an end-to-end test that runs after a production deployment of this service and on a daily basis (scheduled pipeline). There is a followup issue for implementing a similar test in staging, prior to the production deployment ([glgo#39](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/issues/39)) and QA tests on the Rails side should follow ([gitlab#443777](https://gitlab.com/gitlab-org/gitlab/-/issues/443777)).

- [x] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?

  We have an end-to-end test pipeline that is triggered in the glgo repository after each deployment (implemented [here](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/issues/21)). This currently only targets production, not staging ([followup issue](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/issues/39)).


- [x] Will it be possible to roll back this feature? If so explain how it will be possible.

  All the changes that depend on this service are behind a [feature flag](https://gitlab.com/gitlab-org/gitlab/-/issues/443470) on the GitLab Rails side.

### Security

_The items below will be reviewed by the InfraSec team._

- [x] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?

  To be able to compromise the main functionality of this service (OIDC token exchange), an attacker would need to gain access to the private key used by the service to sign output tokens. This private key is set using an environment variable (`GLGO_IDENTITY_KEY`). The value of this variable is currently set using a secret from Vault that is then injected in Cloud Run by Runway ([docs](https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/blob/master/secrets-management.md?ref_type=heads)).

- [x] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

  There is one minor outstanding security-related [issue](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/issues/44).

  We feel comfortable with going into Beta.

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the troubleshooting runbooks.

  There are currently no specific troubleshooting runbooks for this service. We aim to create these as we find the need to.

- [x] Link to an example of an alert and a corresponding runbook.

  Alerts can be found in [Thanos](https://thanos-query.ops.gitlab.net/alerts), searching for `GlgoServiceRunway`. These are currently being routed to the `#g_container_registry_alerts` channel on Slack.

- [x] Confirm that on-call Reliability SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.

  Being managed by Runway, SREs should have access to this service.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [x] Link to notes or testing results for assessing the outcome of failures of individual components.

  As mentioned before, the only dependency of this service is the platform where it runs (Cloud Run) and the private key secret from Vault. If the former fails, the service becomes unavailable. If the latter goes missing, the application won't start.

- [x] What are the potential scalability or performance issues that may result with this change?

  Right now we're using [these](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/main/.runway/runway.yml?ref_type=heads) Runway scalability and capacity settings.

  The rollout is starting with a private (invite only) preview (Experimental) in March. So the starting load will be minimal. We will then progress to a public preview (Beta) in April, and only then it will become generally available later this year. Therefore, we expect to be able to adapt the scalability and capacity settings are we progress through these phases. The service itself is very small and the endpoints extremely fast, so scalability is unlikely to be a concern even in the distant future.

- [x] What are a few operational concerns that will not be present at launch, but may be a concern later?

  The expected request rate is still unknown at this moment. We will evaluate this during the rollout phases.

- [x] Are there any single points of failure in the design? If so list them here.

  The hard dependency on the private signing key secret from Vault.

- [x] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

  As for a possible blast-radius, in case of a service unavailability, the GitLab Rails features that depend on it (see [Overview](#overview)) will become unavailable. No other unrelated features should be affected.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [x] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?

  No.

- [x] How does data age? Can data over a certain age be deleted?

  Stateless service. Docker images for previous versions are available through Runway. At the moment we're not proactively cleaning up old versions.

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [x] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).

  We have not performed any extensive performance testing. The main service endpoint ([token exchange](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/b96c7501b1cbeb2679263c7aa714a15b247126b5/docs/api.md#token-exchange)) is extremely fast, taking ~5ms per request ([source](https://console.cloud.google.com/logs/query;cursorTimestamp=2024-02-23T02:20:56Z;endTime=2024-02-23T09:03:18.446Z;query=resource.type%20%3D%20%22cloud_run_revision%22%0Aresource.labels.service_name%20%3D%20%22glgo%22%0Aresource.labels.location%20%3D%20%22us-east1%22%0AjsonPayload.correlation_id%3D%2201HQ9W82APSXVCP7E9QHBPJ96D%22%0Atimestamp%3D%222024-02-23T02:20:56Z%22%0AinsertId%3D%2265d80108000837dee5f0f2cf%22;startTime=2024-02-22T09:03:18.446Z;summaryFields=:false:32:beginning?project=gitlab-runway-production)). Also considering the gradual rollout (private preview, public preview, GA), we do not expect any sudden performance concerns.
  Long-term capacity planning will be managed by Runway in [Tamland](https://gitlab-com.gitlab.io/gl-infra/capacity-planning-trackers/gitlab-com/service_groups/runway/).

- [x] Link to any load testing plans and results.

  See above.

- [x] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?

  No.

- [x] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.

  The service has a built-in rate limiter, as described [here](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/c4de2f4e1c6b8930e942e3ad07eaea088dfe496b/docs/security.md#rate-limiting). The available and current settings can be found [here](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/c4de2f4e1c6b8930e942e3ad07eaea088dfe496b/docs/configuration.md).

- [x] Are there retry and back-off strategies for external dependencies?

  The Rails backend and Runner (only GitLab clients of this service) do not currently perform any kind of automated retries, but client tools used to interact with Google Cloud services, namely the `gcloud` CLI, may, in which case a new authentication request (and the corresponding token exchange request) may be issued.

- [x] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

  See rate limiting point above.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

  https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17666

- [x] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?

  The service exposes a general health check endpoint ([docs](https://gitlab.com/gitlab-org/architecture/gitlab-gcp-integration/glgo/-/blob/main/docs/api.md?ref_type=heads#health-check)) and apdex and error rate SLIs ([source](https://dashboards.gitlab.net/d/glgo-main/glgo3a-overview?orgId=1)).
- [x] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?

  Yes. The Docker image for each new deployment is built with GitLab CI and stored in the GitLab Container Registry. Deployments are managed by Runway.
